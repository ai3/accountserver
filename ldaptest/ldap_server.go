package ldaptest

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"testing"
	"time"

	"golang.org/x/sync/errgroup"
)

// Config for the test in-memory LDAP server.
type Config struct {
	Dir   string
	Base  string
	LDIFs []string
}

type TestLDAPServer struct {
	tempf string
	proc  *exec.Cmd
	done  chan error

	Addr string
}

var (
	javaBin = "java"
)

func init() {
	if s := os.Getenv("JAVA_HOME"); s != "" {
		javaBin = fmt.Sprintf("%s/bin/java", s)
	}
}

func nextPort() int {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}
	defer l.Close()

	_, portStr, _ := net.SplitHostPort(l.Addr().String())
	port, err := strconv.Atoi(portStr)
	if err != nil {
		panic(err)
	}
	return port
}

func isCanceled(ctx context.Context) (canc bool) {
	select {
	case <-ctx.Done():
		canc = true
	default:
	}
	return
}

func waitForPort(ctx context.Context, port int, timeout time.Duration) error {
	addr := fmt.Sprintf("127.0.0.1:%d", port)
	deadline := time.Now().Add(timeout)
	ctx, cancel := context.WithDeadline(ctx, deadline)
	defer cancel()
	dialer := new(net.Dialer)
	for !isCanceled(ctx) {
		conn, err := dialer.DialContext(ctx, "tcp", addr)
		if err == nil {
			conn.Close()
			log.Printf("test ldap server is up at port %d", port)
			return nil
		}
		time.Sleep(100 * time.Millisecond)
	}
	return ctx.Err()
}

func catLDIFs(w io.Writer, ldifs []string) error {
	for _, path := range ldifs {
		f, err := os.Open(path)
		if err != nil {
			return err
		}
		_, err = io.Copy(w, f)
		f.Close()
		if err != nil {
			return err
		}
		fmt.Fprintf(w, "\n")
	}
	return nil
}

// StartServer starts a test LDAP server with the specified configuration.
func StartServer(t testing.TB, config *Config) *TestLDAPServer {
	port := nextPort()

	tmpf, err := os.CreateTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	if err := catLDIFs(tmpf, config.LDIFs); err != nil {
		t.Fatal(err)
	}
	tmpf.Close()

	proc := exec.Command(
		javaBin,
		"-cp", filepath.Join(config.Dir, "unboundid-ldapsdk", "unboundid-ldapsdk.jar"),
		"com.unboundid.ldap.listener.InMemoryDirectoryServerTool",
		"--port", strconv.Itoa(port),
		"--baseDN", config.Base,
		"--additionalBindDN", fmt.Sprintf("cn=manager,%s", config.Base),
		"--additionalBindPassword", "password",
		"--ldifFile", tmpf.Name(),
	)
	proc.Stdout = os.Stdout
	proc.Stderr = os.Stderr

	if err := proc.Start(); err != nil {
		t.Fatalf("error starting LDAP server: %v", err)
	}

	doneCh := make(chan error)
	go func() {
		doneCh <- proc.Wait()
	}()

	// Wait for the server to start, or to exit with an error.
	eg, ctx := errgroup.WithContext(context.Background())
	started := make(chan bool)

	eg.Go(func() error {
		defer close(started)
		return waitForPort(ctx, port, 5*time.Second)
	})

	eg.Go(func() (err error) {
		select {
		case <-ctx.Done():
		case <-started:
		case err = <-doneCh:
		}
		return
	})

	if err := eg.Wait(); err != nil {
		t.Fatalf("error starting ldap server: %v", err)
	}

	return &TestLDAPServer{
		proc:  proc,
		tempf: tmpf.Name(),
		done:  doneCh,
		Addr:  fmt.Sprintf("ldap://127.0.0.1:%d", port),
	}
}

func (s *TestLDAPServer) Close() {
	s.proc.Process.Kill() //nolint:errcheck
	os.Remove(s.tempf)
}
