//go:build integration

package integrationtest

import (
	"bytes"
	"testing"

	as "git.autistici.org/ai3/accountserver"
	"github.com/ProtonMail/gopenpgp/v3/crypto"
	"github.com/ProtonMail/gopenpgp/v3/profile"
)

func genTestGPGKey(t *testing.T) *crypto.Key {
	pgpDefault := crypto.PGPWithProfile(profile.Default())
	key, err := pgpDefault.KeyGeneration().
		AddUserId("uno", "uno@investici.org").
		Lifetime(86400).
		New().
		GenerateKey()
	if err != nil {
		t.Fatal(err)
	}
	return key
}

func genTestGPGKeyNoExpiry(t *testing.T) *crypto.Key {
	pgpDefault := crypto.PGPWithProfile(profile.Default())
	key, err := pgpDefault.KeyGeneration().
		AddUserId("uno", "uno@investici.org").
		New().
		GenerateKey()
	if err != nil {
		t.Fatal(err)
	}
	return key
}

func TestIntegration_SetNewPGPKey(t *testing.T) {
	runSetNewPGPKeyTest(t, genTestGPGKey(t))
}

func TestIntegration_SetNewPGPKey_NoExpiry(t *testing.T) {
	runSetNewPGPKeyTest(t, genTestGPGKeyNoExpiry(t))
}

func runSetNewPGPKeyTest(t *testing.T, testKey *crypto.Key) {
	stop, _, c := startService(t)
	defer stop()

	rsrcID := as.ResourceID("mail=uno@investici.org,uid=uno@investici.org,ou=People,dc=example,dc=com")
	testKeyData, _ := testKey.GetPublicKey()

	// First, set the key to something new.
	err := c.request("/api/resource/email/set_openpgp_key", &as.SetOpenPGPKeyRequest{
		ResourceRequestBase: as.ResourceRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("uno@investici.org"),
			},
			ResourceID: rsrcID,
		},
		OpenPGPKey: testKeyData,
	}, nil)
	if err != nil {
		t.Fatal(err)
	}

	// Verify that the key is set by calling GetUser.
	var user as.User
	err = c.request("/api/user/get", &as.GetUserRequest{
		UserRequestBase: as.UserRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("uno@investici.org"),
			},
			Username: "uno@investici.org",
		},
	}, &user)
	if err != nil {
		t.Fatal(err)
	}
	email := user.GetResourcesByType(as.ResourceTypeEmail)[0]
	if email.Email.OpenPGPKey == nil {
		t.Fatal("email.OpenPGPKey is nil")
	}
	if !bytes.Equal(email.Email.OpenPGPKey.Key, testKeyData) {
		t.Fatal("email.OpenPGPKey.Key does not match expected data")
	}
	if email.Email.OpenPGPKey.ID != testKey.GetHexKeyID() {
		t.Fatalf("email.OpenPGPKey.ID mismatch: %s, expected %s", email.Email.OpenPGPKey.ID, testKey.GetHexKeyID())
	}
	if email.Email.OpenPGPKey.Hashes[0] != "og5xkb3w4f6n39ysbu6yx5btzg6pfke6@investici.org" {
		t.Fatalf("email.OpenPGPKey.Hash unexpected: %v", email.Email.OpenPGPKey.Hashes)
	}

	// Now unset the key.
	err = c.request("/api/resource/email/set_openpgp_key", &as.SetOpenPGPKeyRequest{
		ResourceRequestBase: as.ResourceRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("uno@investici.org"),
			},
			ResourceID: rsrcID,
		},
	}, nil)
	if err != nil {
		t.Fatal(err)
	}

	// Fetch the user again and see that it has no OpenPGPKey entry.
	err = c.request("/api/user/get", &as.GetUserRequest{
		UserRequestBase: as.UserRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("uno@investici.org"),
			},
			Username: "uno@investici.org",
		},
	}, &user)
	if err != nil {
		t.Fatal(err)
	}
	email = user.GetResourcesByType(as.ResourceTypeEmail)[0]
	if email.Email.OpenPGPKey != nil {
		t.Fatal("email.OpenPGPKey is still set after being deleted")
	}
}
