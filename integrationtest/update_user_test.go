//go:build integration

package integrationtest

import (
	"bytes"
	"testing"

	as "git.autistici.org/ai3/accountserver"
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
)

func TestIntegration_UpdateUser_Lang(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	err := c.request("/api/user/update", &as.UpdateUserRequest{
		UserRequestBase: as.UserRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("quattro@investici.org"),
			},
			Username: "quattro@investici.org",
		},
		SetLang: true,
		Lang:    "zh",
	}, nil)
	if err != nil {
		t.Fatalf("UpdateUser(): %v", err)
	}

	user, err := c.getUser("quattro@investici.org")
	if err != nil {
		t.Fatalf("GetUser(): %v", err)
	}
	if user.Lang != "zh" {
		t.Fatalf("UpdateUser(lang=zh) failed, found lang=%s", user.Lang)
	}
}

func countRegistrations(t *testing.T, c *testClient) (*as.User, int) {
	user, err := c.getUser("quattro@investici.org")
	if err != nil {
		t.Fatalf("GetUser(): %v", err)
	}
	return user, len(user.U2FRegistrations)
}

func TestIntegration_UpdateUser_U2F(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	// Ensure we have loaded the existing registrations correctly.
	if _, n := countRegistrations(t, c); n != 2 {
		t.Fatalf("didn't load existing U2F registrations correctly, expected 2 found %d", n)
	}

	reg := ct.U2FRegistration{
		PublicKey: []byte("haha"),
		KeyHandle: []byte("woof"),
		Comment:   "heya",
	}
	err := c.request("/api/user/update", &as.UpdateUserRequest{
		UserRequestBase: as.UserRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("quattro@investici.org"),
			},
			Username: "quattro@investici.org",
		},
		SetU2FRegistrations: true,
		U2FRegistrations:    []*ct.U2FRegistration{&reg},
	}, nil)
	if err != nil {
		t.Fatalf("UpdateUser(): %v", err)
	}

	user, n := countRegistrations(t, c)
	if n != 1 {
		t.Fatalf("didn't update U2F registrations correctly, expected 1 found %d", n)
	}
	if newReg := user.U2FRegistrations[0]; !registrationEqual(newReg, &reg) {
		t.Errorf("deserialization of registration failed, value=%+v", *newReg)
	}
}

func TestIntegration_UpdateUser_DisableU2F(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	// Removing all hardware tokens should clear 2FA for this user.
	err := c.request("/api/user/update", &as.UpdateUserRequest{
		UserRequestBase: as.UserRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("quattro@investici.org"),
			},
			Username: "quattro@investici.org",
		},
		SetU2FRegistrations: true,
		U2FRegistrations:    nil,
	}, nil)
	if err != nil {
		t.Fatalf("UpdateUser(): %v", err)
	}

	user, n := countRegistrations(t, c)
	if n != 0 {
		t.Fatalf("didn't update U2F registrations correctly, expected 0 found %d", n)
	}
	if n := len(user.AppSpecificPasswords); n > 0 {
		t.Errorf("user still has app-specific passwords (%d)", n)
	}
	if user.Has2FA {
		t.Errorf("user still has Has2FA=true")
	}
}

func registrationEqual(a, b *ct.U2FRegistration) bool {
	return (bytes.Equal(a.PublicKey, b.PublicKey) &&
		bytes.Equal(a.KeyHandle, b.KeyHandle) &&
		a.Comment == b.Comment)
}
