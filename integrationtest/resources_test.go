//go:build integration

package integrationtest

import (
	"encoding/json"
	"errors"
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_GetResource_Auth(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	rid := "cn=example.com,uid=uno@investici.org,ou=People,dc=example,dc=com"
	testdata := []struct {
		authUser   string
		authGroup  string
		resourceID string
		expectedOk bool
	}{
		{"uno@investici.org", "users", rid, false},
		{testAdminUser, testAdminGroup, rid, true},
	}

	for _, td := range testdata {
		var groups []string
		if td.authGroup != "" {
			groups = append(groups, td.authGroup)
		}

		err := c.request("/api/resource/get", &as.GetResourceRequest{
			AdminResourceRequestBase: as.AdminResourceRequestBase{
				ResourceRequestBase: as.ResourceRequestBase{
					RequestBase: as.RequestBase{
						SSO: c.ssoTicket(td.authUser, groups...),
					},
					ResourceID: as.ResourceID(td.resourceID),
				},
			},
		}, nil)
		if td.expectedOk && err != nil {
			t.Errorf("access error for user %s: expected ok, got error: %v", td.authUser, err)
		} else if !td.expectedOk && err == nil {
			t.Errorf("access error for user %s: expected error, got ok", td.authUser)
		}
	}
}

func TestIntegration_SetResourceStatus(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	rid := "cn=example.com,uid=uno@investici.org,ou=People,dc=example,dc=com"

	// User access to owned object is allowed with inactive status.
	err := c.request("/api/resource/set_status", &as.SetResourceStatusRequest{
		ResourceRequestBase: as.ResourceRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("uno@investici.org", "users"),
			},
			ResourceID: as.ResourceID(rid),
		},
		Status: as.ResourceStatusInactive,
	}, nil)
	if err != nil {
		t.Fatalf("SetResourceStatus(owned, inactive): %v", err)
	}

	err = c.request("/api/resource/set_status", &as.SetResourceStatusRequest{
		ResourceRequestBase: as.ResourceRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("uno@investici.org", "users"),
			},
			ResourceID: as.ResourceID(rid),
		},
		Status: as.ResourceStatusActive,
	}, nil)
	if err == nil {
		t.Fatalf("SetResourceStatus(owned, active): err=nil")
	}

	// User access to non-owned object is forbidden.
	err = c.request("/api/resource/set_status", &as.SetResourceStatusRequest{
		ResourceRequestBase: as.ResourceRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("due@investici.org", "users"),
			},
			ResourceID: as.ResourceID(rid),
		},
		Status: as.ResourceStatusInactive,
	}, nil)
	if err == nil {
		t.Fatal("SetResourceStatus(non-owned): err=nil")
	}

	// Admin access is always allowed.
	err = c.request("/api/resource/set_status", &as.SetResourceStatusRequest{
		ResourceRequestBase: as.ResourceRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket(testAdminUser, testAdminGroup),
			},
			ResourceID: as.ResourceID(rid),
		},
		Status: as.ResourceStatusActive,
	}, nil)
	if err != nil {
		t.Fatalf("SetResourceStatus(admin, active): %v", err)
	}
}

func TestIntegration_AdminUpdateResource(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	rid := "cn=example.com,uid=uno@investici.org,ou=People,dc=example,dc=com"

	getResource := func() *as.Resource {
		var rsrc as.GetResourceResponse
		err := c.request("/api/resource/get", &as.GetResourceRequest{
			AdminResourceRequestBase: as.AdminResourceRequestBase{
				ResourceRequestBase: as.ResourceRequestBase{
					RequestBase: as.RequestBase{
						SSO: c.ssoTicket(testAdminUser, testAdminGroup),
					},
					ResourceID: as.ResourceID(rid),
				},
			},
		}, &rsrc)
		if err != nil {
			t.Fatalf("GetResource(): %v", err)
		}
		return rsrc.Resource
	}

	rsrc := getResource()
	if rsrc.Website == nil {
		t.Fatalf("resource has empty Website object: %+v", *rsrc)
	}
	rsrc.Website.AcceptMail = false

	err := c.request("/api/resource/update", &as.AdminUpdateResourceRequest{
		AdminResourceRequestBase: as.AdminResourceRequestBase{
			ResourceRequestBase: as.ResourceRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(testAdminUser, testAdminGroup),
				},
				ResourceID: as.ResourceID(rid),
			},
		},
		Resource: rsrc,
	}, nil)
	if err != nil {
		t.Fatalf("AdminUpdateResource(): %v", err)
	}

	rsrc = getResource()
	if rsrc.Website.AcceptMail {
		t.Fatal("AdminUpdateResource() did not update the acceptMail field")
	}
}

func TestIntegration_AdminUpdateResource_FailValidation(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	rid := "cn=example.com,uid=uno@investici.org,ou=People,dc=example,dc=com"

	var resp as.GetResourceResponse
	err := c.request("/api/resource/get", &as.GetResourceRequest{
		AdminResourceRequestBase: as.AdminResourceRequestBase{
			ResourceRequestBase: as.ResourceRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(testAdminUser, testAdminGroup),
				},
				ResourceID: as.ResourceID(rid),
			},
		},
	}, &resp)
	if err != nil {
		t.Fatalf("GetResource(): %v", err)
	}

	rsrc := resp.Resource
	rsrc.Website.UID = 42999

	err = c.request("/api/resource/update", &as.AdminUpdateResourceRequest{
		AdminResourceRequestBase: as.AdminResourceRequestBase{
			ResourceRequestBase: as.ResourceRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(testAdminUser, testAdminGroup),
				},
				ResourceID: as.ResourceID(rid),
			},
		},
		Resource: rsrc,
	}, nil)
	if err == nil {
		t.Fatal("AdminUpdateResource() accepted faulty update")
	}
	var httpErr *httpError
	if !errors.As(err, &httpErr) {
		t.Fatalf("AdminUpdateResource() did not return a structured error: %v", err)
	}
	var fields map[string][]string
	if err := json.Unmarshal(httpErr.data, &fields); err != nil {
		t.Fatalf("error unmarshaling JSON error: %v", err)
	}
	if values, ok := fields["website.uid"]; !ok || len(values) != 1 {
		t.Fatalf("error does not contain details for the 'website.uid' field: %+v", fields)
	}
}
