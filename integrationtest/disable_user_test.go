//go:build integration

package integrationtest

import (
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_DisableUser(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	username := "uno@investici.org"
	err := c.request("/api/user/disable", &as.DisableUserRequest{
		UserRequestBase: as.UserRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket(username),
			},
			Username: username,
		},
	}, nil)
	if err != nil {
		t.Fatalf("DisableUser(): %v", err)
	}

	// Normal GetUser does not return the user anymore.
	if _, err := c.getUser(username); err == nil {
		t.Fatal("GetUser() returns user after deletion")
	}

	// We have to specify IncludeInactive to get it.
	var user as.User
	err = c.request("/api/user/get", &as.GetUserRequest{
		UserRequestBase: as.UserRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket(testAdminUser, testAdminGroup),
			},
			Username: username,
		},
		IncludeInactive: true,
	}, &user)
	if err != nil {
		t.Fatalf("GetUser(include_inactive=true) error: %v", err)
	}
	if user.Status != as.UserStatusInactive {
		t.Errorf("user status is not inactive: %s", user.Status)
	}
	for _, r := range user.Resources {
		if r.Status != as.ResourceStatusInactive {
			t.Errorf("resource %s status is not inactive: %s", r.ID.String(), r.Status)
		}
	}
}
