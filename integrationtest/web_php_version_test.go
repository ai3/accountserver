//go:build integration

package integrationtest

import (
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_WebSetPHPVersion(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	// The following are basically checks for email validation.
	testdata := []struct {
		phpVersion string
		expectedOk bool
	}{
		{"php", true},
		{"php1.0", true},
		{"php8.2", true},
		{"not a php version", false},
	}

	// Cheat a bit, we know the DN.
	rsrcID := as.ResourceID("alias=due,uid=due@investici.org,ou=People,dc=example,dc=com")

	for _, td := range testdata {
		err := c.request("/api/resource/web/set_php_version", &as.WebSetPHPVersionRequest{
			ResourceRequestBase: as.ResourceRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket("due@investici.org"),
				},
				ResourceID: rsrcID,
			},
			PHPVersion: td.phpVersion,
		}, nil)
		if err == nil && !td.expectedOk {
			t.Errorf("WebSetPHPVersion(%s) should have failed but didn't", td.phpVersion)
		} else if err != nil && td.expectedOk {
			t.Errorf("WebSetPHPVersion(%s) failed: %v", td.phpVersion, err)
		}
	}
}
