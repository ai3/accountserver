//go:build integration

package integrationtest

import (
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_CheckResourceAvailability(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	testdata := []struct {
		rtype         string
		name          string
		expectedAvail bool
	}{
		{"email", "uno@investici.org", false},
		{"email", "due@investici.org", false},
		{"email", "tre@investici.org", false},
		{"email", "newsletter1@investici.org", false},
		{"email", "list4@investici.org", false},
		{"email", "new@investici.org", true},

		{"list", "newsletter1@investici.org", false},
		{"list", "list4@investici.org", false},

		{"newsletter", "newsletter1@investici.org", false},
		{"newsletter", "list4@investici.org", false},

		{"web", "due", false},
		{"web", "new", true},

		{"dav", "uno", false},
		{"dav", "new", true},
	}

	for _, td := range testdata {
		var resp as.CheckResourceAvailabilityResponse
		err := c.request("/api/resource/check_availability", &as.CheckResourceAvailabilityRequest{
			Type: td.rtype,
			Name: td.name,
		}, &resp)
		if err != nil {
			t.Errorf("CheckResourceAvailability(%s/%s) error: %v", td.rtype, td.name, err)
			continue
		}
		if resp.Available != td.expectedAvail {
			t.Errorf("CheckResourceAvailability(%s/%s) returned %v, expected %v", td.rtype, td.name, resp.Available, td.expectedAvail)
		}
	}
}
