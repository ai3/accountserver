//go:build integration

package integrationtest

import (
	"encoding/json"
	"errors"
	"net/http"
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_Error_GetUser(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	testdata := []struct {
		reqUser         string
		authUser        string
		authGroup       string
		expectedErrCode int
	}{
		{"uno@investici.org", "due@investici.org", "", http.StatusForbidden},
		{"zero@investici.org", testAdminUser, testAdminGroup, http.StatusNotFound},
		{"", testAdminUser, testAdminGroup, http.StatusNotFound},
	}

	for _, td := range testdata {
		var user as.User
		var groups []string
		if td.authGroup != "" {
			groups = append(groups, td.authGroup)
		}
		err := c.request("/api/user/get", &as.GetUserRequest{
			UserRequestBase: as.UserRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(td.authUser, groups...),
				},
				Username: td.reqUser,
			},
		}, &user)
		if err == nil {
			t.Errorf("oops, request for %s as %s succeeded unexpectedly", td.reqUser, td.authUser)
			continue
		}
		var httpErr *httpError
		if !errors.As(err, &httpErr) {
			t.Errorf("oops, request for %s as %s returned unexpected error: %v", td.reqUser, td.authUser, err)
			continue
		}
		if httpErr.code != td.expectedErrCode {
			t.Errorf("request for %s as %s returned error code %d, expected %d", td.reqUser, td.authUser, httpErr.code, td.expectedErrCode)
		}
	}

}

func TestIntegration_Error_GetResource(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	testdata := []struct {
		authUser        string
		authGroup       string
		resourceID      string
		expectedErrCode int
	}{
		{"uno@investici.org", "", "cn=example.com,uid=uno@investici.org,ou=People,dc=example,dc=com", http.StatusForbidden},
		{testAdminUser, testAdminGroup, "cn=not-existing.com,uid=uno@investici.org,ou=People,dc=example,dc=com", http.StatusNotFound},
		{testAdminUser, testAdminGroup, "badly,formatted,DN", http.StatusInternalServerError},
	}

	for _, td := range testdata {
		var groups []string
		if td.authGroup != "" {
			groups = append(groups, td.authGroup)
		}
		err := c.request("/api/resource/get", &as.GetResourceRequest{
			AdminResourceRequestBase: as.AdminResourceRequestBase{
				ResourceRequestBase: as.ResourceRequestBase{
					RequestBase: as.RequestBase{
						SSO: c.ssoTicket(td.authUser, groups...),
					},
					ResourceID: as.ResourceID(td.resourceID),
				},
			},
		}, nil)
		if err == nil {
			t.Errorf("oops, request as %s succeeded unexpectedly", td.authUser)
			continue
		}
		var httpErr *httpError
		if !errors.As(err, &httpErr) {
			t.Errorf("oops, request as %s returned unexpected error: %v", td.authUser, err)
			continue
		}
		if httpErr.code != td.expectedErrCode {
			t.Errorf("request as %s returned error code %d, expected %d", td.authUser, httpErr.code, td.expectedErrCode)
		}
	}
}

func TestIntegration_Error_Validation(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	var resp as.CreateUserResponse
	err := c.request("/api/user/create", &as.CreateUserRequest{
		AdminRequestBase: as.AdminRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket(testAdminUser),
			},
		},
		User: &as.User{
			Name: "%@example.com",
			Resources: []*as.Resource{
				&as.Resource{
					Type: as.ResourceTypeEmail,
					Name: "%@example.com",
				},
			},
		},
	}, &resp)
	if err == nil {
		t.Fatal("CreateUser() succeeded unexpectedly")
	}
	var httpErr *httpError
	if !errors.As(err, &httpErr) {
		t.Fatalf("CreateUser() did not return a structured error: %v", err)
	}

	var fields map[string][]string
	if err := json.Unmarshal(httpErr.data, &fields); err != nil {
		t.Fatalf("error unmarshaling JSON error: %v", err)
	}
	if values, ok := fields["name"]; !ok || len(values) != 1 {
		t.Fatalf("error does not contain details for the 'name' field: %+v", fields)
	}
}
