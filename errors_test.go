package accountserver

import (
	"errors"
	"testing"
)

func TestErrors_Nesting(t *testing.T) {
	verr := newValidationErrorStr("field", "bad")
	rerr := newRequestError(verr)

	if !IsRequestError(rerr) {
		t.Errorf("IsRequestError() returned false")
	}
	if !IsValidationError(rerr) {
		t.Errorf("IsValidationError() returned false")
	}

	if !IsValidationError(errors.Join(
		newValidationErrorStr("field1", "bad"),
		newValidationErrorStr("field2", "bad"),
	)) {
		t.Errorf("IsValidationError() does not survive aggregation")
	}
}

func TestErrors_Criticality(t *testing.T) {
	for _, isAdmin := range []bool{false, true} {
		for idx, td := range []struct {
			err  error
			crit bool
		}{
			{errors.New("bare error"), true},
			{newValidationErrorStr("field", "bad"), true},
			{nonCriticalErr(newValidationErrorStr("field", "bad")), false},
			{errors.Join(
				newValidationErrorStr("field1", "bad"),
				newValidationErrorStr("field2", "bad"),
			), true},
			{errors.Join(
				newValidationErrorStr("field1", "bad"),
				nonCriticalErr(newValidationErrorStr("field2", "bad")),
			), true},
			{errors.Join(
				nonCriticalErr(newValidationErrorStr("field1", "bad")),
				nonCriticalErr(newValidationErrorStr("field2", "bad")),
			), false},
		} {
			rctx := &RequestContext{
				Auth: Auth{IsAdmin: isAdmin},
			}
			crit := isCriticalErr(rctx, td.err)

			// All errors are critical for non-admins.
			expectCrit := td.crit
			if !isAdmin {
				expectCrit = true
			}
			if crit != expectCrit {
				t.Errorf("[%d] test failed, crit=%v expected=%v", idx, crit, expectCrit)
			}
		}
	}
}

func TestErrors_CriticalMasking(t *testing.T) {
	rctx := &RequestContext{
		Auth: Auth{IsAdmin: true},
	}

	err := errors.Join(
		newValidationErrorStr("field1", "bad"),
		nonCriticalErr(newValidationErrorStr("field2", "not too bad")),
	)
	if !isCriticalErr(rctx, err) {
		t.Fatal("critical error was masked")
	}

	err = errors.Join(
		nonCriticalErr(newValidationErrorStr("field1", "not too bad")),
		nonCriticalErr(newValidationErrorStr("field2", "not too bad")),
	)
	if isCriticalErr(rctx, err) {
		t.Fatal("non-critical error status was lost in aggregation")
	}
}

func TestErrors_JSON(t *testing.T) {
	verr := newValidationErrorStr("field", "bad")
	rerr := newRequestError(verr)

	data := string(ErrorToJSON(rerr))
	if data != "{\"field\":[\"bad\"]}" {
		t.Errorf("unexpected ErrorToJSON() output: %s", data)
	}
}
