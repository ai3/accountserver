package ldapbackend

import (
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
)

// Note: all functions below deliberately ignore deserialization
// errors (the broken entry is just skipped). This way, broken data in
// the database will be invisible and not break the app. Not that it
// is an overall better strategy than crashing, but it's a bit gentler
// on the user.

func decodeAppSpecificPasswords(values []string) []*ct.AppSpecificPassword {
	var out []*ct.AppSpecificPassword
	for _, value := range values {
		if asp, err := ct.UnmarshalAppSpecificPassword(value); err == nil {
			out = append(out, asp)
		}
	}
	return out
}

func excludeASPFromList(asps []*ct.AppSpecificPassword, id string) []*ct.AppSpecificPassword {
	var out []*ct.AppSpecificPassword
	for _, asp := range asps {
		if asp.ID != id {
			out = append(out, asp)
		}
	}
	return out
}

func encodeAppSpecificPasswords(asps []*ct.AppSpecificPassword) []string {
	var out []string
	for _, asp := range asps {
		out = append(out, asp.Marshal())
	}
	return out
}

func sanitizeAppSpecificPasswords(asps []*ct.AppSpecificPassword) []*ct.AppSpecificPassword {
	for i := 0; i < len(asps); i++ {
		asps[i].EncryptedPassword = ""
	}
	return asps
}

func decodeUserEncryptionKeys(values []string) []*ct.EncryptedKey {
	var out []*ct.EncryptedKey
	for _, value := range values {
		k, err := ct.UnmarshalEncryptedKey(value)
		if err != nil {
			continue
		}
		out = append(out, k)
	}
	return out
}

func encodeUserEncryptionKeys(keys []*ct.EncryptedKey) []string {
	var out []string
	for _, key := range keys {
		out = append(out, key.Marshal())
	}
	return out
}

func decodeU2FRegistrations(encRegs []string) []*ct.U2FRegistration {
	var out []*ct.U2FRegistration
	for _, enc := range encRegs {
		if r, err := ct.UnmarshalU2FRegistration(enc); err == nil {
			out = append(out, r)
		}
	}
	return out
}

func encodeU2FRegistrations(regs []*ct.U2FRegistration) []string {
	var out []string
	for _, r := range regs {
		out = append(out, r.Marshal())
	}
	return out
}
