package cachebackend

import (
	"context"
	"log"
	"net/http"
	"sync"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/go-common/serverutil"
)

const InvalidateURLPath = "/api/internal/cache_invalidate"

var rpcTimeout = 5 * time.Second

type InvalidateUserRequest struct {
	Username string `json:"username"`
}

var emptyResponse = struct{}{}

type replicatedCache struct {
	internalCache

	peers map[string]clientutil.Backend
}

func newCacheWithReplicatedInvalidation(c internalCache, peers []string, tls *clientutil.TLSClientConfig) (*replicatedCache, error) {
	peerBackends := make(map[string]clientutil.Backend)
	for _, peerURL := range peers {
		b, err := clientutil.NewBackend(&clientutil.BackendConfig{
			URL:       peerURL,
			TLSConfig: tls,
		})
		if err != nil {
			return nil, err
		}
		peerBackends[peerURL] = b
	}
	return &replicatedCache{
		internalCache: c,
		peers:         peerBackends,
	}, nil
}

func (rc *replicatedCache) Delete(username string) {
	// Delete the user entry from the local cache.
	rc.internalCache.Delete(username)

	ctx, cancel := context.WithTimeout(context.Background(), rpcTimeout)
	defer cancel()

	// Reach out to all peers in parallel with an InvalidateUserRequest.
	var wg sync.WaitGroup
	for peerURL, peer := range rc.peers {
		wg.Add(1)
		go func(peerURL string, peer clientutil.Backend) {
			defer wg.Done()
			if err := peer.Call(ctx, InvalidateURLPath, "", &InvalidateUserRequest{Username: username}, nil); err != nil {
				log.Printf("error invalidating cache for %s on %s: %v", username, peerURL, err)
			}
		}(peerURL, peer)
	}
	wg.Wait()
}

func (rc *replicatedCache) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var req InvalidateUserRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	// Remove user data from the local cache.
	rc.internalCache.Delete(req.Username)

	serverutil.EncodeJSONResponse(w, emptyResponse)
}
