package accountserver

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"regexp"
)

// A stringSet is just a list of strings with a quick membership test.
type stringSet struct {
	set  map[string]struct{}
	list []string
}

func newStringSetFromList(list []string) *stringSet {
	set := make(map[string]struct{})
	for _, s := range list {
		set[s] = struct{}{}
	}
	return &stringSet{set: set, list: list}
}

func newStringSetFromFileOrList(list []string, path string) (*stringSet, error) {
	if path != "" {
		var err error
		list, err = loadStringsFromFile(path)
		if err != nil {
			return nil, err
		}
	}
	return newStringSetFromList(list), nil
}

func (s *stringSet) Contains(needle string) bool {
	if s == nil {
		return false
	}
	_, ok := s.set[needle]
	return ok
}

func (s *stringSet) List() []string {
	if s == nil {
		return nil
	}
	return s.list
}

// A set of regular expressions.
type regexpSet struct {
	exprs []*regexp.Regexp
}

func newRegexpSetFromList(list []string) (*regexpSet, error) {
	var set regexpSet
	for _, pattern := range list {
		// Automatically hook the pattern to the beg/end of input.
		rx, err := regexp.Compile(fmt.Sprintf("^%s$", pattern))
		if err != nil {
			return nil, err
		}
		set.exprs = append(set.exprs, rx)
	}
	return &set, nil
}

func newRegexpSetFromFileOrList(list []string, path string) (*regexpSet, error) {
	if path != "" {
		var err error
		list, err = loadStringsFromFile(path)
		if err != nil {
			return nil, err
		}
	}
	return newRegexpSetFromList(list)
}

func (s *regexpSet) Contains(needle string) bool {
	for _, rx := range s.exprs {
		if rx.MatchString(needle) {
			return true
		}
	}
	return false
}

// A domainBackend that works with a static list of type-specific allowed domains.
type staticDomainBackend struct {
	sets map[string]*stringSet
}

func (d *staticDomainBackend) GetAllowedDomains(_ context.Context, kind string) []string {
	return d.sets[kind].List()
}

func (d *staticDomainBackend) IsAllowedDomain(_ *RequestContext, kind, domain string) bool {
	return d.sets[kind].Contains(domain)
}

type staticShardBackend struct {
	available map[string]*stringSet
	allowed   map[string]*stringSet
}

func (d *staticShardBackend) GetAllowedShards(_ context.Context, kind string) []string {
	return d.allowed[kind].List()
}

func (d *staticShardBackend) GetAvailableShards(_ context.Context, kind string) []string {
	return d.available[kind].List()
}

func (d *staticShardBackend) IsAllowedShard(_ context.Context, kind, shard string) bool {
	return d.allowed[kind].Contains(shard)
}

func loadStringsFromFile(path string) ([]string, error) {
	f, err := os.Open(path) // #nosec
	if err != nil {
		return nil, err
	}
	defer f.Close() // nolint: errcheck

	var list []string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" || line[0] == '#' {
			continue
		}
		list = append(list, line)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return list, nil
}
