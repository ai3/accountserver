package accountserver

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

// A domainBackend manages the list of domains users are allowed to request services on.
type domainBackend interface {
	GetAllowedDomains(context.Context, string) []string
	IsAllowedDomain(*RequestContext, string, string) bool
}

// A shardBackend can return information about available / allowed service shards.
type shardBackend interface {
	GetAllowedShards(context.Context, string) []string
	GetAvailableShards(context.Context, string) []string
	IsAllowedShard(context.Context, string, string) bool
}

// ValidationConfig specifies a large number of validation-related
// configurable parameters.
type ValidationConfig struct {
	ForbiddenUsernames     []string            `yaml:"forbidden_usernames"`
	ForbiddenUsernamesFile string              `yaml:"forbidden_usernames_file"`
	ForbiddenPasswords     []string            `yaml:"forbidden_passwords"`
	ForbiddenPasswordsFile string              `yaml:"forbidden_passwords_file"`
	ForbiddenDomains       []string            `yaml:"forbidden_domains"`
	ForbiddenDomainsFile   string              `yaml:"forbidden_domains_file"`
	AvailableDomains       map[string][]string `yaml:"available_domains"`
	WebsiteRootDir         string              `yaml:"website_root_dir"`
	MinPasswordLen         int                 `yaml:"min_password_len"`
	MaxPasswordLen         int                 `yaml:"max_password_len"`
	MinUsernameLen         int                 `yaml:"min_username_len"`
	MaxUsernameLen         int                 `yaml:"max_username_len"`
	MinUID                 int                 `yaml:"min_backend_uid"`
	MaxUID                 int                 `yaml:"max_backend_uid"`

	forbiddenUsernames *regexpSet
	forbiddenPasswords *stringSet
	forbiddenDomains   *regexpSet
}

const (
	defaultMinPasswordLen = 8
	defaultMaxPasswordLen = 128
	defaultMinUsernameLen = 3
	defaultMaxUsernameLen = 64
	defaultMinUID         = 1000
	defaultMaxUID         = 0
)

func (c *ValidationConfig) setDefaults() {
	if c.MinPasswordLen == 0 {
		c.MinPasswordLen = defaultMinPasswordLen
	}
	if c.MaxPasswordLen == 0 {
		c.MaxPasswordLen = defaultMaxPasswordLen
	}
	if c.MinUsernameLen == 0 {
		c.MinUsernameLen = defaultMinUsernameLen
	}
	if c.MaxUsernameLen == 0 {
		c.MaxUsernameLen = defaultMaxUsernameLen
	}
	if c.MinUID == 0 {
		c.MinUID = defaultMinUID
	}
	if c.MaxUID == 0 {
		c.MaxUID = defaultMaxUID
	}
}

func (c *ValidationConfig) compile() (err error) {
	c.setDefaults()

	c.forbiddenUsernames, err = newRegexpSetFromFileOrList(c.ForbiddenUsernames, c.ForbiddenUsernamesFile)
	if err != nil {
		return
	}
	c.forbiddenPasswords, err = newStringSetFromFileOrList(c.ForbiddenPasswords, c.ForbiddenPasswordsFile)
	if err != nil {
		return
	}
	c.forbiddenDomains, err = newRegexpSetFromFileOrList(c.ForbiddenDomains, c.ForbiddenDomainsFile)
	return
}

// The validationContext contains all configuration and backends that
// the various validation functions will need. Most methods on this
// object return functions themselves (ValidatorFunc or variations
// thereof) that can later be called multiple times at will and
// combined with operators like 'allOf'.
type validationContext struct {
	config  *ValidationConfig
	domains domainBackend
	shards  shardBackend
	backend Backend
}

// Split out the username part of an email address.
func splitUserFromAddr(addr string) string {
	if n := strings.IndexByte(addr, '@'); n > 0 {
		return addr[:n]
	}
	return addr
}

// Returns all the possible resources in the email and mailing list
// namespaces that might conflict with the given email address.
func relatedEmails(ctx context.Context, be domainBackend, addr string) []FindResourceRequest {
	// Check for the literal addr in the unified mail+list namespace.
	rel := []FindResourceRequest{
		{Type: ResourceTypeEmail, Name: addr},
		{Type: ResourceTypeMailingList, Name: addr},
		{Type: ResourceTypeNewsletter, Name: addr},
	}

	// Mailing lists and newsletters must have unique names
	// regardless of the domain, so we add potential conflicts for
	// mailing lists with the same name over all list-enabled
	// domains.
	user := splitUserFromAddr(addr)
	for _, d := range be.GetAllowedDomains(ctx, ResourceTypeMailingList) {
		rel = append(rel, FindResourceRequest{
			Type: ResourceTypeMailingList,
			Name: fmt.Sprintf("%s@%s", user, d),
		})
	}
	for _, d := range be.GetAllowedDomains(ctx, ResourceTypeNewsletter) {
		rel = append(rel, FindResourceRequest{
			Type: ResourceTypeNewsletter,
			Name: fmt.Sprintf("%s@%s", user, d),
		})
	}
	return rel
}

func relatedWebsites(ctx context.Context, be domainBackend, value string) []FindResourceRequest {
	// Ignore the parent domain (websites share a global namespace).
	return []FindResourceRequest{
		{
			Type: ResourceTypeWebsite,
			Name: value,
		},
	}
}

func relatedDomains(ctx context.Context, be domainBackend, value string) []FindResourceRequest {
	return []FindResourceRequest{
		{
			Type: ResourceTypeDomain,
			Name: value,
		},
	}
}

func (v *validationContext) isAllowedDomain(rtype string) ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		if !v.domains.IsAllowedDomain(rctx, rtype, value) {
			return errors.New("unavailable domain")
		}
		return nil
	}
}

func (v *validationContext) isAllowedEmailDomain() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		// Static lookup first.
		if v.domains.IsAllowedDomain(rctx, ResourceTypeEmail, value) {
			return nil
		}

		// Dynamic lookup for hosted domains in the database
		// that have AcceptMail=true.
		if tx, err := v.backend.NewTransaction(); err == nil {

			// Ignore the error in FindResource() since we're going to fail close anyway.
			// nolint: errcheck
			rsrc, _ := tx.FindResource(rctx.Context, FindResourceRequest{Type: ResourceTypeDomain, Name: value})
			if rsrc != nil && rsrc.Status == ResourceStatusActive && rsrc.Website.AcceptMail {
				return nil
			}
		}

		return errors.New("unavailable domain")
	}
}

func (v *validationContext) isAvailableEmailAddr() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := relatedEmails(rctx.Context, v.domains, value)

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the address unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("address unavailable")
		}
		return nil
	}
}

func (v *validationContext) isAvailableDomain() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := relatedDomains(rctx.Context, v.domains, value)

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the resource unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("address unavailable")
		}
		return nil
	}
}

func (v *validationContext) isAvailableWebsite() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := relatedWebsites(rctx.Context, v.domains, value)

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the resource unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("address unavailable")
		}
		return nil
	}
}

func (v *validationContext) isAvailableDAV() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := []FindResourceRequest{
			{
				Type: ResourceTypeDAV,
				Name: value,
			},
		}

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the resource unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("name unavailable")
		}
		return nil
	}
}

func (v *validationContext) isAvailableDatabase() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := []FindResourceRequest{
			{
				Type: ResourceTypeDatabase,
				Name: value,
			},
		}

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the resource unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("name unavailable")
		}
		return nil
	}
}

func (v *validationContext) validHostedEmail() ValidatorFunc {
	return validateUsernameAndDomain(
		allOf(
			matchUsernameRx(),
			nonCritical(minLength(v.config.MinUsernameLen)),
			nonCritical(maxLength(v.config.MaxUsernameLen)),
			notInSet(v.config.forbiddenUsernames),
		),
		allOf(v.isAllowedEmailDomain()),
	)
}

func (v *validationContext) validHostedNewEmail() ValidatorFunc {
	return allOf(
		v.validHostedEmail(),
		v.isAvailableEmailAddr(),
	)
}

func (v *validationContext) validHostedMailingList() ValidatorFunc {
	return validateUsernameAndDomain(
		allOf(
			matchUsernameRx(),
			nonCritical(minLength(v.config.MinUsernameLen)),
			nonCritical(maxLength(v.config.MaxUsernameLen)),
			notInSet(v.config.forbiddenUsernames),
		),
		allOf(v.isAllowedDomain(ResourceTypeMailingList)),
	)
}

func (v *validationContext) validPassword() ValidatorFunc {
	return allOf(
		minLength(v.config.MinPasswordLen),
		maxLength(v.config.MaxPasswordLen),
		notInSet(v.config.forbiddenPasswords),
	)
}

func allOf(funcs ...ValidatorFunc) ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		for _, f := range funcs {
			if err := f(rctx, value); err != nil {
				return err
			}
		}
		return nil
	}
}

// ResourceValidatorFunc is a composite type validator that checks
// various fields in a Resource, depending on its type. If it returns
// an error, it is supposed to be a ValidationError, which is a
// composite error type that can hold field-specific error details. A
// bare error is considered as an "internal error" (consistency
// issues, anything the caller can't do anything about).
type ResourceValidatorFunc func(*RequestContext, *Resource, *User, bool) error

func (v *validationContext) validateBaseResource(_ *RequestContext, r *Resource, user *User) error {
	// Validate the status enum.
	switch r.Status {
	case ResourceStatusActive, ResourceStatusInactive, ResourceStatusReadonly, ResourceStatusArchived:
	default:
		return newValidationErrorStr("status", "unknown resource status")
	}

	// If the resource has a ParentID, it must reference another
	// resource owned by the user.
	if !r.ParentID.Empty() {
		if user == nil {
			return newValidationErrorStr("parent_id", "resource can't have parent without user context")
		}
		if p := user.GetResourceByID(r.ParentID); p == nil {
			return newValidationErrorStr("parent_id", "parent references unknown resource")
		}
	}

	return nil
}

func (v *validationContext) validateShardedResource(rctx *RequestContext, r *Resource, user *User) error {
	if err := v.validateBaseResource(rctx, r, user); err != nil {
		return err
	}
	if r.Shard == "" {
		return newValidationErrorStr("shard", "empty shard")
	}
	if !v.shards.IsAllowedShard(rctx.Context, r.Type, r.Shard) {
		return newValidationError("shard", fmt.Errorf(
			"invalid shard %s for resource type %s (allowed: %v)",
			r.Shard,
			r.Type,
			v.shards.GetAllowedShards(rctx.Context, r.Type),
		))
	}
	if r.OriginalShard == "" {
		return newValidationErrorStr("original_shard", "empty original_shard")
	}
	return nil
}

func (v *validationContext) validEmailResource() ResourceValidatorFunc {
	emailValidator := v.validHostedEmail()
	newEmailValidator := v.validHostedNewEmail()

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// Email resources aren't nested.
		if !r.ParentID.Empty() {
			return newValidationErrorStr("parent_id", "resource should not have parent")
		}

		if r.Email == nil {
			return newValidationErrorStr("email", "resource has no email metadata")
		}

		var err error
		if isNew {
			err = newEmailValidator(rctx, r.Name)
		} else {
			err = emailValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError("name", err)
		}

		if r.Email.Maildir == "" {
			return newValidationErrorStr("email.maildir", "empty maildir")
		}
		return nil
	}
}

func (v *validationContext) validListResource() ResourceValidatorFunc {
	listValidator := v.validHostedMailingList()
	newListValidator := allOf(listValidator, v.isAvailableEmailAddr())

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}
		if r.List == nil {
			return newValidationErrorStr("list", "resource has no list metadata")
		}

		var err error
		if isNew {
			err = newListValidator(rctx, r.Name)
		} else {
			err = listValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError("name", err)
		}

		if len(r.List.Admins) < 1 {
			return newValidationErrorStr("list.admins", "can't create a list without admins")
		}
		return nil
	}
}

func (v *validationContext) validNewsletterResource() ResourceValidatorFunc {
	listValidator := v.validHostedMailingList()
	newListValidator := allOf(listValidator, v.isAvailableEmailAddr())

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}
		if r.Newsletter == nil {
			return newValidationErrorStr("newsletter", "resource has no newsletter metadata")
		}

		var err error
		if isNew {
			err = newListValidator(rctx, r.Name)
		} else {
			err = listValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError("name", err)
		}

		if len(r.Newsletter.Admins) < 1 {
			return newValidationErrorStr("newsletter.admins", "can't create a newsletter without admins")
		}
		return nil
	}
}

func findMatchingDAVAccount(user *User, r *Resource) *Resource {
	for _, dav := range user.GetResourcesByType(ResourceTypeDAV) {
		if isSubdir(dav.DAV.Homedir, r.Website.DocumentRoot) {
			return dav
		}
	}
	return nil
}

func hasMatchingDAVAccount(user *User, r *Resource) bool {
	return findMatchingDAVAccount(user, r) != nil
}

func (v *validationContext) validateUID(uid int, user *User) error {
	if uid == 0 {
		return errors.New("uid is not set")
	}
	if uid < v.config.MinUID || (v.config.MaxUID > 0 && uid > v.config.MaxUID) {
		return fmt.Errorf("uid %d outside of allowed range (%d-%d)", uid, v.config.MinUID, v.config.MaxUID)
	}
	if user != nil && uid != user.UID {
		return errors.New("uid of resource differs from uid of user")
	}
	return nil
}

func (v *validationContext) validateWebsiteCommon(r *Resource, user *User) error {
	// Document root checks: must not be empty, and the
	// user must have a DAV account with a home directory
	// that is a parent of this document root.
	if r.Website.DocumentRoot == "" {
		return newValidationErrorStr("document_root", "empty document_root")
	}
	if !isSubdir(v.config.WebsiteRootDir, r.Website.DocumentRoot) {
		return newValidationErrorStr("document_root", "document root outside of web root")
	}
	if !hasMatchingDAVAccount(user, r) {
		return newValidationErrorStr("website", "website has no matching DAV account")
	}

	// UID checks.
	if err := v.validateUID(r.Website.UID, user); err != nil {
		return newValidationError("website.uid", err)
	}
	return nil
}

func (v *validationContext) validDomainResource() ResourceValidatorFunc {
	domainValidator := allOf(
		nonCritical(minLength(6)),
		validDomainName,
		notInSet(v.config.forbiddenDomains),
	)
	newDomainValidator := allOf(
		domainValidator,
		v.isAvailableDomain(),
	)

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// Web resources aren't nested.
		if !r.ParentID.Empty() {
			return errors.New("resource should not have parent")
		}

		if r.Website == nil {
			return errors.New("resource has no website metadata")
		}

		var err error
		if isNew {
			err = newDomainValidator(rctx, r.Name)
		} else {
			err = domainValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError("name", err)
		}

		if r.Website.ParentDomain != "" {
			return newValidationErrorStr("parent_domain", "non-empty parent_domain on domain resource")
		}

		return v.validateWebsiteCommon(r, user)
	}
}

func (v *validationContext) validWebsiteResource() ResourceValidatorFunc {
	nameValidator := allOf(
		nonCritical(minLength(6)),
		matchSitenameRx(),
	)
	newNameValidator := allOf(
		nameValidator,
		v.isAvailableWebsite(),
	)
	parentValidator := v.isAllowedDomain(ResourceTypeWebsite)

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// Web resources aren't nested.
		if !r.ParentID.Empty() {
			return errors.New("resource should not have parent")
		}

		if r.Website == nil {
			return errors.New("resource has no website metadata")
		}

		var err error
		if isNew {
			err = newNameValidator(rctx, r.Name)
		} else {
			err = nameValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError("name", err)
		}
		if err := parentValidator(rctx, r.Website.ParentDomain); err != nil {
			return err
		}

		return v.validateWebsiteCommon(r, user)
	}
}

func (v *validationContext) validDAVResource() ResourceValidatorFunc {
	davValidator := allOf(
		nonCritical(minLength(4)),
		matchUsernameRx(),
	)
	newDAVValidator := allOf(
		davValidator,
		v.isAvailableDAV(),
	)
	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// DAV resources aren't nested.
		if !r.ParentID.Empty() {
			return errors.New("resource should not have parent")
		}

		var err error
		if isNew {
			err = newDAVValidator(rctx, r.Name)
		} else {
			err = davValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError("name", err)
		}

		if r.DAV == nil {
			return errors.New("resource has no dav metadata")
		}
		if !isSubdir(v.config.WebsiteRootDir, r.DAV.Homedir) {
			return newValidationErrorStr("homedir", "homedir outside of web root")
		}

		if err := v.validateUID(r.DAV.UID, user); err != nil {
			return newValidationError("dav.uid", err)
		}
		return nil
	}
}

func (v *validationContext) validDatabaseResource() ResourceValidatorFunc {
	dbValidator := allOf(
		nonCritical(minLength(4)),
		matchIdentifierRx(),
	)
	newDBValidator := allOf(
		dbValidator,
		v.isAvailableDatabase(),
	)
	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// Database resources must be nested below a website.
		if r.ParentID.Empty() {
			return errors.New("database resources should be nested")
		}

		// TODO: move to global validation
		// switch r.ParentID.Type() {
		// case ResourceTypeWebsite, ResourceTypeDomain:
		// default:
		// 	return errors.New("database parent is not a website resource")
		// }

		var err error
		if isNew {
			err = newDBValidator(rctx, r.Name)
		} else {
			err = dbValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError("name", err)
		}

		if r.Database == nil {
			return errors.New("resource has no database metadata")
		}
		return nil
	}
}

// Validator for arbitrary resource types.
type resourceValidator struct {
	rvs map[string]ResourceValidatorFunc
}

func (v *validationContext) newResourceValidator() *resourceValidator {
	return &resourceValidator{
		rvs: map[string]ResourceValidatorFunc{
			ResourceTypeEmail:       v.validEmailResource(),
			ResourceTypeMailingList: v.validListResource(),
			ResourceTypeNewsletter:  v.validNewsletterResource(),
			ResourceTypeDomain:      v.validDomainResource(),
			ResourceTypeWebsite:     v.validWebsiteResource(),
			ResourceTypeDAV:         v.validDAVResource(),
			ResourceTypeDatabase:    v.validDatabaseResource(),
		},
	}
}

func (v *resourceValidator) validateResource(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
	// Obvious basic sanity checks on the Resource parameters.
	if r.Name == "" {
		return newValidationErrorStr("name", "resource name unset")
	}
	if r.Type == "" {
		return newValidationErrorStr("type", "resource type unset")
	}

	rv, ok := v.rvs[r.Type]
	if !ok {
		return newValidationError("type", fmt.Errorf("unknown resource type '%s'", r.Type))
	}

	return rv(rctx, r, user, isNew)
}

// UserValidatorFunc is a compound validator for User objects.
type UserValidatorFunc func(*RequestContext, *User, bool) error

// Verify that user-level invariants are respected. This check can be applied
// to new or existing objects.
//
// nolint: gocyclo
func checkUserInvariants(user *User) error {
	// Count email resources. An account must have exactly 1 email
	// resource.
	var email *Resource
	var emailCount int
	for _, rsrc := range user.Resources {
		if rsrc.Type == ResourceTypeEmail {
			email = rsrc
			emailCount++
		}
	}
	if emailCount == 0 {
		return errors.New("account is missing email resource")
	}
	if emailCount > 1 {
		return errors.New("account can't have more than one email resource")
	}
	if email.Name != user.Name {
		return errors.New("email and username do not match")
	}

	// Check UID.
	if user.UID == 0 {
		return errors.New("UID unset")
	}
	// Check that all UIDs are the same.
	for _, rsrc := range user.Resources {
		switch rsrc.Type {
		case ResourceTypeWebsite, ResourceTypeDomain:
			if rsrc.Website.UID != user.UID {
				return fmt.Errorf("UID of resource %s does not match user (%d vs %d)", rsrc.String(), rsrc.Website.UID, user.UID)
			}
		case ResourceTypeDAV:
			if rsrc.DAV.UID != user.UID {
				return fmt.Errorf("UID of resource %s does not match user (%d vs %d)", rsrc.String(), rsrc.DAV.UID, user.UID)
			}
		}
	}

	return nil
}

// A custom validator for new User objects.
func (v *validationContext) validUser() UserValidatorFunc {
	nameValidator := v.validHostedEmail()
	newNameValidator := v.validHostedNewEmail()
	return func(rctx *RequestContext, user *User, isNew bool) error {
		var err error
		if isNew {
			err = newNameValidator(rctx, user.Name)
		} else {
			err = nameValidator(rctx, user.Name)
		}
		if err != nil {
			return err
		}
		return checkUserInvariants(user)
	}
}

// Common validators for specific field types, that can be used by
// Validate() methods on action handlers.
type fieldValidators struct {
	password ValidatorFunc
	newEmail ValidatorFunc
}

func (v *validationContext) newFieldValidators() *fieldValidators {
	return &fieldValidators{
		password: v.validPassword(),
		newEmail: v.validHostedNewEmail(),
	}
}

func isSubdir(root, dir string) bool {
	return strings.HasPrefix(dir, root+"/")
}
