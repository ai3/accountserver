package accountserver

import (
	"errors"
	"fmt"
	"regexp"
	"strings"

	"golang.org/x/net/publicsuffix"
)

// ValidatorFunc is the generic interface for unstructured data field
// (string) validators.
type ValidatorFunc func(*RequestContext, string) error

type genericSet interface {
	Contains(string) bool
}

func notInSet(set genericSet) ValidatorFunc {
	return func(_ *RequestContext, value string) error {
		if set.Contains(value) {
			return errors.New("invalid value (blacklisted)")
		}
		return nil
	}
}

func minLength(minLen int) ValidatorFunc {
	return func(_ *RequestContext, value string) error {
		if len(value) < minLen {
			return fmt.Errorf("value must be at least %d characters", minLen)
		}
		return nil
	}
}

func maxLength(maxLen int) ValidatorFunc {
	return func(_ *RequestContext, value string) error {
		if len(value) > maxLen {
			return fmt.Errorf("value must be at most %d characters", maxLen)
		}
		return nil
	}
}

func matchRegexp(rx *regexp.Regexp, errmsg string) ValidatorFunc {
	return func(_ *RequestContext, value string) error {
		if !rx.MatchString(value) {
			return errors.New(errmsg)
		}
		return nil
	}
}

// The generic username regexp allows for standard usernames.
var usernameRx = regexp.MustCompile(`^([a-z0-9]+[-_.]?)+[a-z0-9]$`)

func matchUsernameRx() ValidatorFunc {
	return matchRegexp(usernameRx, "value must be an alphanumeric sequence, including . and - characters but not starting or ending with those")
}

func matchSitenameRx() ValidatorFunc {
	return matchUsernameRx()
}

// The identifier regexp is stricter and forbids [-_.] characters.
var identifierRx = regexp.MustCompile(`^[a-z0-9]+$`)

func matchIdentifierRx() ValidatorFunc {
	return matchRegexp(identifierRx, "value must be alphanumeric")
}

func validateUsernameAndDomain(validateUsername, validateDomain ValidatorFunc) ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		parts := strings.SplitN(value, "@", 2)
		if len(parts) != 2 {
			return errors.New("malformed email address")
		}
		if err := validateUsername(rctx, parts[0]); err != nil {
			return err
		}
		return validateDomain(rctx, parts[1])
	}
}

// This is not a prescriptive domain name validation regex, it's just used to
// give useful feedback to the user if the domain name looks visibly wrong.
var domainRx = regexp.MustCompile(`^(?:[a-zA-Z0-9_\-]{1,63}\.)+(?:[a-zA-Z]{2,})$`)

func isRegistered(domain string) bool {
	// TODO: ha ha, there isn't really a good way to check.
	return true
}

func validDomainName(_ *RequestContext, value string) error {
	if !domainRx.MatchString(value) {
		return errors.New("invalid domain name")
	}

	// Extract the top-level domain and check that it is registered
	// (which works also if we're hosting the top-level domain
	// ourselves already).
	domain, err := publicsuffix.EffectiveTLDPlusOne(value)
	if err != nil {
		return errors.New("invalid TLD")
	}
	if !isRegistered(domain) {
		return fmt.Errorf("the domain %s does not seem to be registered", domain)
	}
	return nil
}
