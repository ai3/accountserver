package accountserver

import (
	"bytes"
	"crypto/sha1"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/ProtonMail/gopenpgp/v3/crypto"
	"github.com/tv42/zbase32"
)

// SetOpenPGPKeyRequest allows users to set their own OpenPGP keys.
type SetOpenPGPKeyRequest struct {
	ResourceRequestBase

	// Set to empty value to delete key.
	OpenPGPKey []byte `json:"openpgp_key"`

	key               *crypto.Key
	matchedIdentities []string
}

var pgpArmor = []byte("-----BEGIN PGP PUBLIC KEY BLOCK-----")

func splitAddress(addr string) (local, domain string, err error) {
	parts := strings.Split(addr, "@")
	if len(parts) != 2 {
		return "", "", errors.New("wkd: invalid email address")
	}
	return parts[0], parts[1], nil
}

// hashLocal returns the WKD hash for the local part of a given email address.
func hashLocal(local string) string {
	local = strings.ToLower(local)
	hashedLocal := sha1.Sum([]byte(local))
	return zbase32.EncodeToString(hashedLocal[:])
}

// wkdHashAddress combines the WKD hash for the local part of a given
// email address, with the domain, to generate a site-wide unique
// identifier for quick WKD lookup.
func wkdHashAddress(addr string) (string, error) {
	local, domain, err := splitAddress(addr)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s@%s", hashLocal(local), domain), nil
}

// Bag of strings, unordered.
type addrSet map[string]struct{}

func newAddrSet(l []string) addrSet {
	s := make(addrSet)
	for _, elem := range l {
		s.add(elem)
	}
	return s
}

func (s addrSet) add(elem string) {
	s[elem] = struct{}{}
}

func (s addrSet) toList() []string {
	out := make([]string, 0, len(s))
	for elem := range s {
		out = append(out, elem)
	}
	return out
}

func (s addrSet) intersect(b addrSet) addrSet {
	out := make(addrSet)
	for elem := range s {
		if _, ok := b[elem]; ok {
			out.add(elem)
		}
	}
	return out
}

// Find unique identities (email addresses) associated with the key
// that match email addresses (including aliases) of the Resource.
func findMatchingKeyIdentities(rsrc *Resource, key *crypto.Key) []string {
	keyIdentities := newAddrSet(nil)
	for _, identity := range key.GetEntity().Identities {
		// Some keys can have an empty Email field, but will
		// contain an email address in the Id field.
		idEmail := identity.UserId.Email
		if idEmail == "" && strings.Contains(identity.UserId.Id, "@") {
			idEmail = identity.UserId.Id
		}

		keyIdentities.add(idEmail)
	}

	emails := newAddrSet(rsrc.Email.Aliases)
	emails.add(rsrc.Name)

	return emails.intersect(keyIdentities).toList()
}

func parseOpenPGPKey(data []byte) (key *crypto.Key, err error) {
	// Accept either armored or raw inputs.
	if bytes.HasPrefix(data, pgpArmor) {
		key, err = crypto.NewKeyFromArmored(string(data))
	} else {
		key, err = crypto.NewKey(data)
	}
	if err != nil {
		return
	}

	// Detect uploads of private keys!
	if key.IsPrivate() {
		err = errors.New("input is a private key")
	}

	return
}

func getPGPKeyExpiry(key *crypto.Key) int64 {
	ent := key.GetEntity()
	if sig, _ := ent.PrimaryIdentity(time.Now(), nil); sig != nil && sig.KeyLifetimeSecs != nil {
		return ent.PrimaryKey.CreationTime.Add(
			time.Duration(*sig.KeyLifetimeSecs) * time.Second).Unix()
	}
	// Key does not expire.
	return 0
}

func newOpenPGPKey(identities []string, key *crypto.Key) (*OpenPGPKey, error) {
	data, err := key.GetPublicKey()
	if err != nil {
		return nil, err
	}

	var hashes []string
	for _, identity := range identities {
		wkdHash, err := wkdHashAddress(identity)
		if err != nil {
			return nil, err
		}
		hashes = append(hashes, wkdHash)
	}

	return &OpenPGPKey{
		Key:    data,
		ID:     key.GetHexKeyID(),
		Hashes: hashes,
		Expiry: getPGPKeyExpiry(key),
	}, nil
}

func (r *SetOpenPGPKeyRequest) Validate(rctx *RequestContext) error {
	if rctx.Resource.Type != ResourceTypeEmail {
		return newValidationErrorStr("type", "this operation only works on email resources")
	}

	// If a key is present, validate it.
	if len(r.OpenPGPKey) > 0 {
		key, err := parseOpenPGPKey(r.OpenPGPKey)
		if err != nil {
			return newValidationError("openpgp_key", err)
		}

		// Find all the key identities that match the resource
		// email address or one of its aliases. We will later
		// define wkd hash lookup keys for each one of them.
		matchedIdentities := findMatchingKeyIdentities(rctx.Resource, key)
		if len(matchedIdentities) == 0 {
			return newValidationError("openpgp_key", fmt.Errorf(
				"no matching key identity for user %s",
				rctx.Resource.Name))
		}

		r.key = key
		r.matchedIdentities = matchedIdentities
	}

	return nil
}

func (r *SetOpenPGPKeyRequest) Serve(rctx *RequestContext) (interface{}, error) {
	var auditMsg string

	rsrc := rctx.Resource
	if r.key == nil {
		rsrc.Email.OpenPGPKey = nil
		auditMsg = "deleted GPG key"
	} else {
		pgpKey, err := newOpenPGPKey(r.matchedIdentities, r.key)
		if err != nil {
			return nil, err
		}
		rsrc.Email.OpenPGPKey = pgpKey
		auditMsg = fmt.Sprintf("updated GPG key %s", pgpKey.ID)
	}

	if err := rctx.TX.UpdateResource(rctx.Context, rsrc); err != nil {
		return nil, err
	}

	rctx.audit.Log(rctx, rctx.Resource, auditMsg)
	return nil, nil
}
