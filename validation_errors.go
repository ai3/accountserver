package accountserver

import (
	"encoding/json"
	"errors"
	"fmt"
)

// ValidationError represents a field-specific error.
type ValidationError struct {
	field string
	err   error
}

func (v *ValidationError) Error() string {
	return fmt.Sprintf("\"%s\": %s", v.field, v.err.Error())
}

func (v *ValidationError) Unwrap() error { return v.err }

func (v *ValidationError) Is(target error) bool {
	_, ok := target.(*ValidationError)
	return ok
}

func newValidationError(field string, err error) error {
	if field == "" {
		field = "_form"
	}
	return &ValidationError{
		field: field,
		err:   err,
	}
}

func newValidationErrorStr(field, s string) error {
	return newValidationError(field, errors.New(s))
}

// IsValidationError returns true if err is a validation error.
func IsValidationError(err error) bool {
	return errors.Is(err, new(ValidationError))
}

type wrappedError interface {
	Unwrap() error
}

type wrappedErrorMulti interface {
	Unwrap() []error
}

func unwrapErrToMap(err error, out map[string][]string) {
	if t, ok := err.(*ValidationError); ok {
		out[t.field] = append(out[t.field], t.err.Error())
	} else if t, ok := err.(wrappedErrorMulti); ok {
		for _, innerErr := range t.Unwrap() {
			unwrapErrToMap(innerErr, out)
		}
	} else if t, ok := err.(wrappedError); ok {
		unwrapErrToMap(t.Unwrap(), out)
	} else {
		out["error"] = append(out["error"], err.Error())
	}
}

// ErrorToJSON returns a nice JSON object representation of an error
// by unwrapping it and checking for ValidationErrors that might be
// field-specific.
func ErrorToJSON(err error) []byte {
	values := make(map[string][]string)
	unwrapErrToMap(err, values)
	data, _ := json.Marshal(values)
	return data
}

// Some validation errors are non-critical, i.e. they can be safely
// overridden if the request is made by an administrator.
type nonCriticalValidationError struct {
	wrap error
}

func (o *nonCriticalValidationError) Error() string { return o.wrap.Error() }

func (o *nonCriticalValidationError) Unwrap() error { return o.wrap }

func (o *nonCriticalValidationError) Is(target error) bool {
	_, ok := target.(*nonCriticalValidationError)
	return ok
}

func nonCriticalErr(err error) error {
	return &nonCriticalValidationError{wrap: err}
}

func nonCritical(f ValidatorFunc) ValidatorFunc {
	return func(rctx *RequestContext, s string) error {
		if err := f(rctx, s); err != nil {
			return nonCriticalErr(err)
		}
		return nil
	}
}

func unwrapCriticalErrs(err error) bool {
	if _, ok := err.(*nonCriticalValidationError); ok {
		return false
	} else if t, ok := err.(wrappedErrorMulti); ok {
		for _, innerErr := range t.Unwrap() {
			if crit := unwrapCriticalErrs(innerErr); crit {
				return true
			}
		}
		return false
	} else if t, ok := err.(wrappedError); ok {
		return unwrapCriticalErrs(t.Unwrap())
	}
	return true
}

// Returns true if none of the wrapped errors are non-critical.
func isCriticalErr(rctx *RequestContext, err error) bool {
	return !(rctx.Auth.IsAdmin && !unwrapCriticalErrs(err))
}
