API
===

This document defines a generic API to expose user accounts and
associated resources to UIs and management tools, so that they can
delegate privileged operations to a dedicated service.

The focus is on making adding resources and functionality as simple as
possible, to support diverse service ecosystems.

We'd like to make it possible to create a generic, extensible UI for
account management, which would deal with the UX workflows, leaving
the implementation-specific details to the (custom) accountserver
service. Such application would initially support standard operations
on resources: adding functionality should require only adding a bit of
UI-related code to the application (and the implementation-specific
support on the server side).

## Data model

The accountserver data model provides a high-level representation of
user accounts and associated resources, decoupled from the underlying
database implementation (which likely consists of inter-related SQL
tables or LDAP objects). Such high-level representation is targeted at
humans, either users themselves or administrators.

The top-level object is an *account*, representing a user's identity.
Accounts contain a number of *resources* (of different types), which
it owns - meaning that it has administrative control over them.

Resources can be of different types (mailboxes, websites, mailing
lists, etc), each associated with a different service. In some cases,
resources can themselves contain other resources, to represent a close
association of some form (the semantics will be service-specific, an
example could be a website and its associated MySQL database, which
should be co-located).

Resources have a *name*, which must be globally unique for resources
of the same type. Resources within an account are uniquely identified
by an *ID*, which combines type and name and is a globally unique
identifier:

    <id> = <type>/<name>

The resource type `account` identifies the account itself.

Each resource type supports a number of *actions* that can modify the
state of resources. The specific actions and their semantics will need
to be documented (see [Schema](#schema) below), and need to be agreed
upon by the UIs/tools and the accountserver, but this scheme is easily
extensible.

## Interface

The accountserver provides an HTTP(S) endpoint operating as a RPC
server.

Request bodies should be JSON-encoded, same as responses, and must
have a Content-Type of *application/json*.

In case of errors, either with the request (400), or with the server
or its backends (500), the response body will contain a short error
message in text form.

### Authentication

All requests should be authenticated. We assume that there is a way to
identify certain users as *administrators* (for instance via group
membership). Action handlers in the accountserver implementation have
the option of choosing between two different ACL checks (and only
these two):

* *user*:
  * allow a user access if the resource belongs to the user's own
    account
  * allow administrators access to this action for any account
* *admin-only*:
  * only administrators have access to this action

In more detail, there are two options that could be implemented,
depending on the specifics of the authentication system used:

* *Trust the UI and tools*: authenticate the connection with the
  accountserver, and trust the front-ends with their own internal
  access control checks (so that, for instance, user x can not modify
  data belonging to user y).
* *End-to-end verification*: pass an authentication token for the user
  from the front-ends to the accountserver, and verify it on every
  request. This puts the trust on the authentication system itself,
  regardless of potential issues in the front-end implementation.


## Schema

The *schema* describes the known resource types and the actions that
can be performed on them, along with their parameters.

What follows is a sample schema, every resource type is described along with
the attributes that it supports and the actions that can be performed
on it. The relative types are defined in [types.go](types.go).

### user

Represents a user account, or an *identity*. It is the primary
identificator for users in the system.

Attributes:

* `name` - name of the user
* `status` - one of *active*, *inactive*, or other states that might be
  necessary to implement more complex state machines related to
  account activation
* `has_2fa` - true if 2FA is enabled (whether OTP or U2F)
* `u2f_registrations`: list of U2F registration IDs (not the keys
  themselves though)
* `app_specific_passwords` - list of application-specific passwords
  (without the passwords), each with the following attributes:
  * `id` - ID of the password (used to delete it)
  * `comment` - user-written comment identifying this client
* `resources` - list of resources associated with the user

### resource

A resource represents an account with a specific service. Every
resource has a unique *ID* (a path-like structure), and a *type* to
identify which service it is associated with. Resources can be *owned*
by a user, in which case their ID will contain the user's name, or
they can be *global* and not being strictly associated with an
individual users (such is the case of mailing lists, for example).

Also, most resources in a partition-based service are *sharded*, that
is, they are associated with a specific *shard* identifying the host
holding the user's data.

Resources have a number of common attributes, and some type-specific
attributes. The common ones are:

* `id` - the resource ID. IDs are basically opaque (they must make
  sense to the backend, not to users), and should only be used with
  this API. The only exception to the opaqueness is that the first
  path component of the ID is the resource *type*.
* `name` - resource name (for display purposes)
* `parent_id` - the parent resource ID, for nested resources (like
  databases).
* `status` - resource status (active, inactive, etc)
* `shard` - current shard
* `original_shard` - old shard, this is irrelevant to the user except
  that if *shard* != *original_shard*, we may want to communicate that
  the account is being moved by the automation.
* `group` - some resources are grouped together for both display and
  practical purposes (some operations will apply to all them as a
  whole). This is an opaque pseudo-random identifier that, in a list
  of resources, will be set to identify one or more groups of
  resources.

Type-specific attributes are included in a dictionary named after the
resource type itself, for instance:

```
{
  "id": "email/test@example.com/...",
  "email": {
    "aliases": ["test2@example.com]
  },
  ...
}
```

A list of resource types and their attributes is included below.

#### email

Email-specific attributes:

* `aliases` - a list of aliases that will receive email for the same
  mailbox.
* `maildir` - base path for the mailbox storage
* `quota_limit` - quota limit (not implemented)
* `quota_usage` - quota usage (not implemented)

#### list

Represents a mailing list.

* `admins` - list of admin email addresses (whether internal or external)
* `public` - if true, the list archives will be public

#### website, domain

*Websites* are sub-domains of a hosting domain, while *domains* are
hosted on their own public DNS domain (and have associated DNS
entries, etc).

* `url` - public URL for the site
* `parent_domain` - for websites only, the parent domain
* `accept_mail` - for domains, whether to accept email and allow the
  creation of mailbox accounts on this domain.
* `options` - list of options (which are just strings with special
  meaning, documented elsewhere).
* `document_root` - document root of the website

#### dav

A WebDAV account is associated with a home directory hosting one or
more sites.

* `homedir` - home directory for this hosting account

#### database

Databases have a database name and a database user.

* `db_user` - name of the user associated with the database


### Example

This is an example of a simple user, as returned by `/api/user/get`:

```
{
    "name": "uno@example.com",
    "lang": "it",
    "uid": 19475,
    "has_2fa": false,
    "has_otp": false,
    "has_encryption_keys": true,
    "password_recovery_hint": "bla bla bla",
    "u2f_registrations": null,
    "resources": [
        {
            "id": "web/uno@example.com/uno",
            "type": "web",
            "name": "autistici.org/uno",
            "status": "active",
            "shard": "host2",
            "original_shard": "host2",
            "group": "uno",
            "website": {
                "url": "https://www.autistici.org/uno/",
                "uid": 19475,
                "parent_domain": "autistici.org",
                "accept_mail": false,
                "quota_usage": 0,
                "document_root": "/home/users/example.com/uno/html-uno"
            }
        },
        {
            "id": "db/uno@example.com/alias=uno/unodb",
            "type": "db",
            "name": "unodb",
            "parent_id": "web/uno@example.com/uno",
            "status": "active",
            "shard": "host2",
            "original_shard": "host2",
            "group": "uno",
            "database": {
                "db_user": "unodb",
            }
        },
        {
            "id": "domain/uno@example.com/example.com",
            "type": "domain",
            "name": "example.com",
            "status": "active",
            "shard": "host2",
            "original_shard": "host2",
            "group": "uno",
            "website": {
                "url": "https://example.com/",
                "uid": 19475,
                "accept_mail": true,
                "quota_usage": 0,
                "document_root": "/home/users/example.com/uno/html-example.com"
            }
        },
        {
            "id": "dav/uno@example.com/uno",
            "type": "dav",
            "name": "uno",
            "status": "active",
            "shard": "host2",
            "original_shard": "host2",
            "group": "uno",
            "dav": {
                "uid": 19475,
                "homedir": "/home/users/example.com/uno"
            }
        },
        {
            "id": "email/uno@example.com/uno@example.com",
            "type": "email",
            "name": "uno@example.com",
            "status": "active",
            "shard": "host2",
            "original_shard": "host2",
            "email": {
                "maildir": "example.com/uno/",
                "quota_limit": 0,
                "quota_usage": 0
            }
        }
    ]
}
```


# API Reference

## User endpoints

The following API endpoints invoke operations on an individual
user. Access is allowed for admins, and for the user itself.

Most requests dealing with encryption keys are so-called *privileged*
requests, and will require the user's password in the *cur_password*
parameter in order to decrypt the existing encryption keys.

### `/api/user/get`

Retrieve information about a user and all its associated resources.

Request parameters:

* `username` - user to fetch
* `sso` - SSO ticket

### `/api/user/change_password`

Change the primary authentication password for a user. This operation
will update the user's storage encryption keys, or initialize them if
they do not exist.

Request parameters:

* `username` - name of the user
* `sso` - SSO ticket
* `cur_password` - current valid password for the user
* `password` - new password (unencrypted)

### `/api/user/reset_password`

Admin operation to forcefully reset the password for an account. This
operation will disable all 2FA credentials and it will re-generate the
storage encryption keys. The user will lose access to existing data.

Request parameters:

* `username` - name of the user
* `sso` - SSO ticket

Response parameters:

* `password` - the new random password

### `/api/user/set_password_recovery_hint`

Sets the secondary authentication password (a hint / response pair,
used to recover the primary credentials) for a user. This operation
will update the user's storage encryption keys, or initialize them if
they do not exist yet.

Request parameters:

* `username` - name of the user
* `sso` - SSO ticket
* `cur_password` - current valid password for the user
* `hint` - the secondary authentication hint
* `response` - the secondary authentication response (effectively a
  password, unencrypted)

### `/api/user/enable_otp`

Enable TOTP for a user. The server can generate a new TOTP secret if
necessary, or it can be generated by the caller (usually better as it
allows for a better validation UX).

Request parameters:

* `username` - name of the user
* `sso` - SSO ticket
* `totp_secret` - new TOTP secret (optional)

### `/api/user/disable_otp`

Disable TOTP for a user. Existing 2FA credentials will be wiped as
well.

Request parameters:

* `username` - name of the user
* `sso` - SSO ticket

### `/api/user/create_app_specific_password`

Create a new application-specific password. 2FA must already be
enabled for the user. A new random password will be generated by the
server and returned in the response. A new copy of the encryption key
will be encrypted with the new application-specific password.

ASPs are identified by a unique random ID that is also automatically
generated by the server.

Request parameters:

* `username` - name of the user
* `sso` - SSO ticket
* `cur_password` - current valid password for the user
* `service` - service that the password should be valid for
* `notes` - a user-controlled comment about the client

### `/api/user/delete_app_specific_password`

Delete an application-specific password (and the associated encryption
key).

Request parameters:

* `username` - name of the user
* `sso` - SSO ticket
* `asp_id` - ID of the app-specific password

### `/api/user/update`

Allows the caller to update a (very limited) selected set of fields on
a user object. It is a catch-all function for very simple changes that
don't justify their own specialized method.

Non-authentication request attributes are all optional.

Request parameters:

* `username` - user to modify
* `sso` - SSO ticket
* `lang` - set the preferred language for this user
* `u2f_registrations` - set the list of U2F registrations

### `/api/user/admin_update`

A privileged version of the above (admin-only), that allows updates to
user status and other privileged internal attributes.

Request parameters:

* `username` - user to modify
* `sso` - SSO ticket
* `status` - if not empty, new user status

### `/api/user/create`

Create a new user (admin-only). Will also create all the resources
that are included in the request. A large number of attributes on the
user and resource objects can be safely omitted, as they will be
filled in by the server.

The returned response will include the password for the new user (in
cleartext).

Request parameters:

* `sso` - SSO ticket
* `user` - user to create, with resources

### `/api/recover_account`

Special endpoint for account recovery, used by the login FE service
(sso-server) to retrieve the password recovery hint, and to trigger
password recovery with the user-provided recovery password.

The recovery workflow should look like this: first, the sso-server
sends a PasswordRecoveryRequest with only the *username* field set,
retrieving the password recovery hint for the user. Then it presents
the hint to the user, and asks for both the recovery password and the
new password to be set if recovery is successful. These are finally
sent to the accountserver in another PasswordRecoveryRequest.

Request parameters:

* `username` - user name
* `recovery_password` - recovery password. Account recovery will only
  be attempted if this field is not empty, otherwise we'll just return
  the password hint.
* `password` - new password to be set for the account

Response parameters:

* `hint` - the password recovery hint to show the user

## Resource endpoints

These API endpoints manipulate individual resources regardless of
which user they belong to. Access is normally granted to admins and to
the user that owns a resource, but some operations are restricted to
admins only.

### `/api/resource/set_status`

Modify the status of a resource (admin-only).

Request parameters:

* `resource_id` - resource ID
* `sso` - SSO ticket
* `status` - new status (one of *active*, *inactive*, *readonly*
  or *archived*, depending on the resource type)

### `/api/resource/move`

Move a resource between shards (admin-only).

Resources that are part of a group (for instance websites and DAV
accounts) are moved together, so this request might end up moving more
than one resource.

Request parameters:

* `resource_id` - resource ID
* `sso` - SSO ticket
* `shard` - new shard

### `/api/resource/create`

Create one or more resources associated with a user. Note that if
creating multiple resources, they must all belong to the same user.

Request parameters:

* `sso` - SSO ticket
* `resources` - list of resource objects to create

### `/api/resource/check_availability`

Verify if a resource (identified here by type and name) exists or not,
with the purpose of providing early feedback to applications creating
new services.

Request parameters:

* `type` - resource type
* `name` - resource name

Response attributes:

* `available` - bool


## Type-specific resource endpoints

These API endpoints are specific to resources of certain types.

### `/api/resource/reset_password`

Reset the password associated with a resource (if the resource type
supports it). It will generate a random password and return it in
cleartext in the response.

Request parameters:

* `resource_id` - resource ID
* `sso` - SSO ticket

### `/api/resource/email/add_alias`

Add a new alias to an email resource.

Request parameters:

* `resource_id` - resource ID
* `sso` - SSO ticket
* `addr` - email address of the new alias

### `/api/resource/email/delete_alias`

Delete an alias from an email resource.

Request parameters:

* `resource_id` - resource ID
* `sso` - SSO ticket
* `addr` - email address of the alias to delete

