package accountserver

import (
	"context"
	"testing"
)

type validationTestData struct {
	value string
	ok    bool
}

func runFieldValidationTest(t testing.TB, v ValidatorFunc, testData []validationTestData) {
	for _, td := range testData {
		err := v(&RequestContext{Context: context.Background()}, td.value)
		if (err == nil && !td.ok) || (err != nil && td.ok) {
			t.Errorf("test for '%s' failed: expected %v, got error %v", td.value, td.ok, err)
		}
	}
}

func TestValidator_NotInSet(t *testing.T) {
	td := []validationTestData{
		{"apple", true},
		{"pear", false},
		{"", true},
	}
	runFieldValidationTest(t, notInSet(newStringSetFromList([]string{"pear", "banana"})), td)
}

func TestValidator_MinMaxLength(t *testing.T) {
	td := []validationTestData{
		{"a", false},
		{"aaa", true},
		{"aaaaa", true},
		{"aaaaaaa", false},
		{"aaaaaaaaa", false},
	}
	runFieldValidationTest(t, allOf(minLength(2), maxLength(5)), td)
}

func TestValidator_MatchUsername(t *testing.T) {
	td := []validationTestData{
		{"a", false},
		{"aa", true},
		{"abc", true},
		{"123", true},
		{"a-b", true},
		{"a_b", true},
		{"a-b-c-d", true},
		{"-ab", false},
		{"ab-", false},
		{"a--b", false},
		{"a__b", false},
		{"...", false},
		{"a.b", true},
		{"a.-b", false},
		{"a-.-b", false},
	}
	runFieldValidationTest(t, matchUsernameRx(), td)
}
