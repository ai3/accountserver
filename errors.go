package accountserver

import (
	"errors"
)

var (
	// ErrUserNotFound is returned when a user object is not found.
	ErrUserNotFound = errors.New("user not found")

	// ErrResourceNotFound is returned when a resource object is not found.
	ErrResourceNotFound = errors.New("resource not found")
)

// It is important to distinguish between different classes of errors,
// so that they can be translated into distinct HTTP status codes and
// transmitted back to the client. Since we also want to retain the
// original error message, we wrap the original error with our custom
// types.

// The API error type is a wrapper with a specific class (code).
type apiError struct {
	code int
	wrap error
}

func (e *apiError) Error() string { return e.wrap.Error() }

func (e *apiError) Unwrap() error { return e.wrap }

func newAPIError(code int, err error) *apiError {
	return &apiError{code: code, wrap: err}
}

func isAPIErrorWithCode(err error, code int) bool {
	var ae *apiError
	return errors.As(err, &ae) && ae.code == code
}

const (
	codeAuthErr = iota
	codeRequestErr
	codeBackendErr
)

func newAuthError(err error) error {
	return newAPIError(codeAuthErr, err)
}

// IsAuthError returns true if err is an authentication /
// authorization error.
func IsAuthError(err error) bool {
	return isAPIErrorWithCode(err, codeAuthErr)
}

func newRequestError(err error) error {
	return newAPIError(codeRequestErr, err)
}

// IsRequestError returns true if err is a request error (bad
// request).
func IsRequestError(err error) bool {
	return isAPIErrorWithCode(err, codeRequestErr)
}

func newBackendError(err error) error {
	return newAPIError(codeBackendErr, err)
}

// IsBackendError returns true if err is a backend error.
func IsBackendError(err error) bool {
	return isAPIErrorWithCode(err, codeBackendErr)
}
