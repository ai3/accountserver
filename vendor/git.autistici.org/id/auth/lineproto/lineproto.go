package lineproto

import (
	"context"
	"errors"
	"io"
	"log"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// LineHandler is the handler for LineServer.
type LineHandler interface {
	ServeLine(context.Context, LineResponseWriter, []byte) error
}

// ErrCloseConnection must be returned by a LineHandler when we want
// to cleanly terminate the connection without raising an error.
var ErrCloseConnection = errors.New("close")

// LineResponseWriter writes a single-line response to the underlying
// connection.
type LineResponseWriter interface {
	// WriteLine writes a response as a single line (the line
	// terminator is added by the function).
	WriteLine(...[]byte) error
}

// LineServer implements a line-based text protocol. It satisfies the
// Handler interface.
type LineServer struct {
	handler LineHandler

	IdleTimeout    time.Duration
	WriteTimeout   time.Duration
	RequestTimeout time.Duration
}

var (
	defaultIdleTimeout    = 600 * time.Second
	defaultWriteTimeout   = 10 * time.Second
	defaultRequestTimeout = 30 * time.Second
)

// NewLineServer returns a new LineServer with the given handler and
// default I/O timeouts.
func NewLineServer(h LineHandler) *LineServer {
	return &LineServer{
		handler:        h,
		IdleTimeout:    defaultIdleTimeout,
		WriteTimeout:   defaultWriteTimeout,
		RequestTimeout: defaultRequestTimeout,
	}
}

// ServeConnection handles a new connection. It will accept multiple
// requests on the same connection (or not, depending on the client
// preference).
func (l *LineServer) ServeConnection(c *Conn) {
	totalConnections.WithLabelValues(c.ServerName).Inc()
	for {
		c.Conn.SetReadDeadline(time.Now().Add(l.IdleTimeout)) // nolint
		line, err := c.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			if !strings.Contains(err.Error(), "use of closed network connection") {
				log.Printf("client error: %v", err)
			}
			break
		}

		// Create a context for the request and call the
		// handler with it.  Set a write deadline on the
		// connection to allow the full RequestTimeout time to
		// generate the response.
		start := time.Now()
		c.Conn.SetWriteDeadline(start.Add(l.RequestTimeout + l.WriteTimeout)) // nolint
		ctx, cancel := context.WithTimeout(context.Background(), l.RequestTimeout)
		err = l.handler.ServeLine(ctx, c, line)
		elapsedMs := time.Since(start).Nanoseconds() / 1000000
		requestLatencyHist.WithLabelValues(c.ServerName).
			Observe(float64(elapsedMs))
		cancel()

		// Close the connection on error, or on empty response.
		if err != nil {
			totalRequests.WithLabelValues(c.ServerName, "error").Inc()
			if err != ErrCloseConnection {
				log.Printf("request error: %v", err)
			}
			break
		}
		totalRequests.WithLabelValues(c.ServerName, "ok").Inc()
	}
}

// Instrumentation metrics.
var (
	totalConnections = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "lineproto_connections_total",
			Help: "Total number of connections.",
		},
		[]string{"listener"},
	)
	totalRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "lineproto_requests_total",
			Help: "Total number of requests.",
		},
		[]string{"listener", "status"},
	)
	// Histogram buckets are tuned for the low-milliseconds range
	// (the largest bucket sits at ~1s).
	requestLatencyHist = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "lineproto_requests_latency_ms",
			Help:    "Latency of requests.",
			Buckets: prometheus.ExponentialBuckets(5, 1.4142, 16),
		},
		[]string{"listener"},
	)
)

func init() {
	prometheus.MustRegister(totalConnections)
	prometheus.MustRegister(totalRequests)
	prometheus.MustRegister(requestLatencyHist)

}
