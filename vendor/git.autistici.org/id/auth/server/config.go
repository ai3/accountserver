package server

import (
	"log"
	"path/filepath"
	"sort"

	"git.autistici.org/ai3/go-common/clientutil"
	"gopkg.in/yaml.v3"

	"git.autistici.org/id/auth/backend"
)

// ServiceConfig defines the authentication backends for a service.
type ServiceConfig struct {
	BackendSpecs             []*backend.Spec `yaml:"backends"`
	ChallengeResponse        bool            `yaml:"challenge_response"`
	Enforce2FA               bool            `yaml:"enforce_2fa"`
	Ignore2FA                bool            `yaml:"ignore_2fa"`
	EnableLastLoginReporting bool            `yaml:"enable_last_login_reporting"`
	EnableDeviceTracking     bool            `yaml:"enable_device_tracking"`
	Ratelimits               []string        `yaml:"rate_limits"`
	ASPService               string          `yaml:"asp_service"`
}

// Config for the authentication server.
type Config struct {
	// Service-specific configuration.
	Services map[string]*ServiceConfig `yaml:"services"`

	// If set, load more service definitions from *.yml files in this directory.
	ServicesDir string `yaml:"services_dir"`

	// Enabled backends.
	Backends map[string]yaml.Node `yaml:"backends"`

	// If set, load more backend definitions from *.yml files in this directory.
	BackendsDir string `yaml:"backends_dir"`

	// Named rate limiter configurations.
	RateLimiters map[string]*authRatelimiterConfig `yaml:"rate_limits"`

	// Configuration for the user-meta-server backend.
	UserMetaDBConfig *clientutil.BackendConfig `yaml:"user_meta_server"`

	// Memcache servers used for short-term storage.
	MemcacheServers []string `yaml:"memcache_servers"`

	// WebAuthN configuration.
	WebAuthn struct {
		RPDisplayName string `yaml:"rp_display_name"`
		RPID          string `yaml:"rp_id"`
		RPOrigin      string `yaml:"rp_origin"`
	} `yaml:"webauthn"`

	// Full path to the configuration file, used to resolve relative paths.
	path string
}

// Load a standalone service configuration: a YAML-encoded file that
// may contain one or more ServiceConfig definitions.
func loadStandaloneServiceConfig(path string) (map[string]*ServiceConfig, error) {
	var out map[string]*ServiceConfig
	if err := backend.LoadYAML(path, &out); err != nil {
		return nil, err
	}
	return out, nil
}

// Load a standalone service configuration: a YAML-encoded file that
// may contain one or more ServiceConfig definitions.
func loadStandaloneBackendConfig(path string) (map[string]yaml.Node, error) {
	var out map[string]yaml.Node
	if err := backend.LoadYAML(path, &out); err != nil {
		return nil, err
	}
	return out, nil
}

// Sort a string slice.
type filesList []string

func (l filesList) Len() int           { return len(l) }
func (l filesList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l filesList) Less(i, j int) bool { return l[i] < l[j] }

// Apply a function to all files matching a glob pattern (errors are ignored).
// Files are visited in sorted order.
func forAllFiles(pattern string, f func(string)) {
	files, err := filepath.Glob(pattern)
	if err != nil {
		return
	}
	sort.Sort(filesList(files))
	for _, file := range files {
		f(file)
	}
}

// LoadConfig loads the configuration from a YAML-encoded file.
func LoadConfig(path string) (*Config, error) {
	config := Config{
		path: path,

		// The 'file' backend is always enabled.
		Backends: map[string]yaml.Node{
			"file": yaml.Node{},
		},
	}
	// Defaults for WebAuthn config are only supposed to
	// let the program start without issues, they will not
	// result in a usable WebAuthN setup.
	config.WebAuthn.RPDisplayName = "localhost"
	config.WebAuthn.RPID = "localhost"
	config.WebAuthn.RPOrigin = "localhost"

	if err := backend.LoadYAML(path, &config); err != nil {
		return nil, err
	}

	// Load backend definitions from BackendsDir.
	if config.BackendsDir != "" {
		forAllFiles(filepath.Join(config.BackendsDir, "*.yml"), func(f string) {
			tmp, ferr := loadStandaloneBackendConfig(f)
			if ferr != nil {
				log.Printf("configuration error: %s: %v", f, ferr)
				return
			}
			for name, b := range tmp {
				config.Backends[name] = b
			}
		})
	}

	// Services shouldn't be nil from here onwards.
	if config.Services == nil {
		config.Services = make(map[string]*ServiceConfig)
	}

	// Load service definitions from a directory if necessary, and
	// merge them into config.Services.
	if config.ServicesDir != "" {
		forAllFiles(filepath.Join(config.ServicesDir, "*.yml"), func(f string) {
			tmp, ferr := loadStandaloneServiceConfig(f)
			if ferr != nil {
				log.Printf("configuration error: %s: %v", f, ferr)
				return
			}
			for name, svc := range tmp {
				config.Services[name] = svc
			}
		})
	}

	return &config, nil
}
