package server

import "fmt"

const otpReplayProtectionTTL = 300

type otpStorage struct {
	cacheClient
}

func newOTPStorage(cache cacheClient) *otpStorage {
	return &otpStorage{cache}
}

func (m *otpStorage) AddToken(username, token string) error {
	return m.writeAll(otpMemcacheKey(username, token), []byte{1}, otpReplayProtectionTTL)
}

func (m *otpStorage) HasToken(username, token string) bool {
	_, ok := m.readAny(otpMemcacheKey(username, token))
	return ok
}

func otpMemcacheKey(username, token string) string {
	return fmt.Sprintf("otp/%s/%s", username, token)
}
