package pwhash

import (
	"crypto/subtle"
	"fmt"

	"github.com/amoghe/go-crypt"
)

// systemCryptPasswordHash uses the glibc crypt function.
type systemCryptPasswordHash struct {
	hashStr string
}

// NewSystemCrypt returns a PasswordHash that uses the system crypt(3)
// function, specifically glibc with its SHA512 algorithm.
func NewSystemCrypt() PasswordHash {
	return &systemCryptPasswordHash{"$6$"}
}

// ComparePassword returns true if the given password matches the
// encrypted one.
func (s *systemCryptPasswordHash) ComparePassword(encrypted, password string) bool {
	enc2, err := crypt.Crypt(password, encrypted)
	if err != nil {
		return false
	}
	return subtle.ConstantTimeCompare([]byte(encrypted), []byte(enc2)) == 1
}

// Encrypt the given password using glibc crypt.
func (s *systemCryptPasswordHash) Encrypt(password string) string {
	salt := fmt.Sprintf("%s%x$", s.hashStr, getRandomBytes(16))
	enc, err := crypt.Crypt(password, salt)
	if err != nil {
		panic(err)
	}
	return enc
}
