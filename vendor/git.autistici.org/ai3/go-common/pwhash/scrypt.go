package pwhash

import (
	"crypto/subtle"
	"encoding/hex"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/crypto/scrypt"
)

var (
	scryptKeyLen  = 32
	scryptSaltLen = 16
)

// ScryptPasswordHash uses the scrypt hashing algorithm.
type scryptPasswordHash struct {
	params scryptParams
}

// NewScrypt returns a PasswordHash that uses the scrypt algorithm
// with the default parameters.
func NewScrypt() PasswordHash {
	return NewScryptWithParams(
		defaultScryptParams.N,
		defaultScryptParams.R,
		defaultScryptParams.P,
	)
}

// NewScryptWithParams returns a PasswordHash that uses the scrypt
// algorithm with the specified parameters.
func NewScryptWithParams(n, r, p int) PasswordHash {
	return &scryptPasswordHash{
		params: scryptParams{
			N: n,
			R: r,
			P: p,
		},
	}
}

// ComparePassword returns true if the given password matches
// the encrypted one.
func (s *scryptPasswordHash) ComparePassword(encrypted, password string) bool {
	params, salt, dk, err := decodeScryptHash(encrypted)
	if err != nil {
		return false
	}
	dk2, err := scrypt.Key([]byte(password), salt, params.N, params.R, params.P, scryptKeyLen)
	if err != nil {
		return false
	}
	return subtle.ConstantTimeCompare(dk, dk2) == 1
}

// Encrypt the given password with the scrypt algorithm.
func (s *scryptPasswordHash) Encrypt(password string) string {
	salt := getRandomBytes(scryptSaltLen)

	dk, err := scrypt.Key([]byte(password), salt, s.params.N, s.params.R, s.params.P, scryptKeyLen)
	if err != nil {
		panic(err)
	}

	return encodeScryptHash(s.params, salt, dk)
}

type scryptParams struct {
	N int
	R int
	P int
}

var defaultScryptParams = scryptParams{
	N: 16384,
	R: 8,
	P: 1,
}

func encodeScryptHash(params scryptParams, salt, dk []byte) string {
	return fmt.Sprintf("$s$%d$%d$%d$%x$%x", params.N, params.R, params.P, salt, dk)
}

func decodeScryptHash(s string) (params scryptParams, salt []byte, dk []byte, err error) {
	if !strings.HasPrefix(s, "$s$") {
		err = errors.New("not a scrypt password hash")
		return
	}

	parts := strings.SplitN(s[3:], "$", 5)
	if len(parts) != 5 {
		err = errors.New("bad encoding")
		return
	}

	if params.N, err = strconv.Atoi(parts[0]); err != nil {
		return
	}

	if params.R, err = strconv.Atoi(parts[1]); err != nil {
		return
	}
	if params.P, err = strconv.Atoi(parts[2]); err != nil {
		return
	}

	if salt, err = hex.DecodeString(parts[3]); err != nil {
		return
	}
	dk, err = hex.DecodeString(parts[4])
	return
}
