package accountserver

import (
	"context"
	"fmt"
	"testing"
)

type fakeCheckBackend struct {
	// We need this just to satisfy the Backend interface.
	*fakeBackend

	m map[string]struct{}
}

type fakeCheckTX struct {
	TX
	m map[string]struct{}
}

func newFakeCheckBackend(seeds []FindResourceRequest) *fakeCheckBackend {
	f := &fakeCheckBackend{
		fakeBackend: createFakeBackend(),
		m:           make(map[string]struct{}),
	}
	for _, s := range seeds {
		f.m[fmt.Sprintf("%s/%s", s.Type, s.Name)] = struct{}{}
	}
	return f
}

func (f *fakeCheckBackend) NewTransaction() (TX, error) {
	tx, _ := f.fakeBackend.NewTransaction()
	return &fakeCheckTX{TX: tx, m: f.m}, nil
}

func (f *fakeCheckTX) HasAnyResource(_ context.Context, reqs []FindResourceRequest) (bool, error) {
	for _, req := range reqs {
		if _, ok := f.m[fmt.Sprintf("%s/%s", req.Type, req.Name)]; ok {
			return true, nil
		}
	}
	return false, nil
}

func newTestValidationContext(forbiddenUsernames ...string) *validationContext {
	config := &ValidationConfig{
		ForbiddenUsernames: forbiddenUsernames,
	}
	config.compile() // nolint: errcheck
	return &validationContext{config: config}
}

func newFakeDomainBackend(domains ...string) domainBackend {
	set := newStringSetFromList(domains)
	return &staticDomainBackend{
		sets: map[string]*stringSet{
			ResourceTypeEmail:       set,
			ResourceTypeMailingList: set,
			ResourceTypeWebsite:     set,
		},
	}
}

func newFakeShardBackend(shards ...string) shardBackend {
	b := &staticShardBackend{
		available: make(map[string]*stringSet),
		allowed:   make(map[string]*stringSet),
	}
	for _, kind := range []string{
		ResourceTypeEmail,
		ResourceTypeWebsite,
		ResourceTypeDomain,
	} {
		b.available[kind] = newStringSetFromList(shards)
		b.allowed[kind] = newStringSetFromList(shards)
	}
	return b
}

func TestValidator_HostedEmail(t *testing.T) {
	td := []validationTestData{
		{"user1@example.com", true},
		{"user2@example.com", true},
		{".@example.com", false},
		{"user@other-domain.com", false},
		{"forbidden@example.com", false},
		{"existing@example.com", false},
	}
	vc := newTestValidationContext("forbidden")
	vc.backend = newFakeCheckBackend([]FindResourceRequest{
		{Type: ResourceTypeEmail, Name: "existing@example.com"},
	})
	vc.domains = newFakeDomainBackend("example.com")
	vf := allOf(vc.validHostedEmail(), vc.isAvailableEmailAddr())
	runFieldValidationTest(t, vf, td)
}

func TestValidator_HostedMailingList(t *testing.T) {
	td := []validationTestData{
		{"list1@domain1.com", true},
		{"list2@domain2.com", true},
		{".@domain1.com", false},
		{"list@other-domain.com", false},
		{"forbidden@domain1.com", false},
		{"existing@domain1.com", false},
		{"existing@domain2.com", false},
	}
	vc := newTestValidationContext("forbidden")
	vc.backend = newFakeCheckBackend([]FindResourceRequest{
		{Type: ResourceTypeMailingList, Name: "existing@domain2.com"},
	})
	vc.domains = newFakeDomainBackend("domain1.com", "domain2.com")
	vf := allOf(vc.validHostedMailingList(), vc.isAvailableEmailAddr())
	runFieldValidationTest(t, vf, td)
}

type resourceValidationTestData struct {
	resource *Resource
	auth     Auth
	isNew    bool
	ok       bool
}

func runResourceValidationTest(t testing.TB, rv *resourceValidator, testData []resourceValidationTestData) {
	for idx, td := range testData {
		rctx := &RequestContext{Context: context.Background(), Auth: td.auth}
		user := &User{Name: td.auth.Username, UID: 1234}
		err := rv.validateResource(rctx, td.resource, user, td.isNew)

		// Filter out non-critical errors.
		if err != nil && !isCriticalErr(rctx, err) {
			err = nil
		}

		if (err == nil && !td.ok) || (err != nil && td.ok) {
			t.Errorf("[%d] test for resource %+v failed: expected %v, got error %v",
				idx, *td.resource, td.ok, err)
		}
	}
}

func TestValidator_EmailResource(t *testing.T) {
	vc := newTestValidationContext("forbidden")
	vc.backend = newFakeCheckBackend([]FindResourceRequest{
		{Type: ResourceTypeMailingList, Name: "existing@domain2.com"},
	})
	vc.domains = newFakeDomainBackend("domain1.com", "domain2.com")
	vc.shards = newFakeShardBackend("1", "2")

	rv := vc.newResourceValidator()

	user := Auth{Username: "testuser", IsAdmin: false}
	admin := Auth{Username: "adminuser", IsAdmin: true}

	td := []resourceValidationTestData{
		// New email request, well-formed.
		{
			&Resource{
				Type:          ResourceTypeEmail,
				Name:          "foo@domain1.com",
				Status:        ResourceStatusActive,
				Shard:         "1",
				OriginalShard: "1",
				Email: &Email{
					Maildir: "/home/mail/domain1.com/foo",
				},
			},
			user,
			true,
			true,
		},

		// New email request, failing a minLength check on the username.
		{
			&Resource{
				Type:          ResourceTypeEmail,
				Name:          "ff@domain1.com",
				Status:        ResourceStatusActive,
				Shard:         "1",
				OriginalShard: "1",
				Email: &Email{
					Maildir: "/home/mail/domain1.com/ff",
				},
			},
			user,
			true,
			false,
		},

		// New email request, failing a minLength check on the
		// username, but validation succeeds because we're
		// admin.
		{
			&Resource{
				Type:          ResourceTypeEmail,
				Name:          "ff@domain1.com",
				Status:        ResourceStatusActive,
				Shard:         "1",
				OriginalShard: "1",
				Email: &Email{
					Maildir: "/home/mail/domain1.com/ff",
				},
			},
			admin,
			true,
			true,
		},
	}

	runResourceValidationTest(t, rv, td)
}
